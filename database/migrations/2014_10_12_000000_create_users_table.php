<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('nombre', 60);
            $table->string('ape_pat', 60)->nullable();
            $table->string('ape_mat', 60)->nullable();

            $table->string('username', 60)->unique();

            $table->boolean('is_active')->nullable()->defalut(1);
            $table->boolean('is_password_updated')->nullable()->defalut(0);

            $table->string('email', 320)->unique();
            $table->timestamp('email_verified_at')->nullable();

            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
