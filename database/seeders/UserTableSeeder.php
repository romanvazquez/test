<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->nombre = 'Román';
        $user->ape_pat = 'Vázquez';
        $user->ape_mat = 'Almazán';

        $user->username = 'romanvazquez';
        $user->email = 'rdjvazqueza@gmail.com';
        $user->password = \Illuminate\Support\Facades\Hash::make('password');
        $user->save();
    }
}
