<?php

use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PostCategoriesPageActionController;
use App\Http\Controllers\PostCategoryController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RolePageActionController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('sign-up', function () {
    return view('sign-up');
});

Route::get('verify-employee', function () {
    return view('verify-employee');
});

Route::get('/foo/{id}', [UserController::class, 'foo'])->name('foo.index');

// Route::put('/foo/{modelos}', [UserController::class, 'updateModelos'])->name('modelos.update');
Route::put('/foo1/{requisitos}', [UserController::class, 'updateRequisitos'])->name('requisitos.update');
Route::put('/foo2/{fundamento}', [UserController::class, 'updateFundamento'])->name('fundamento.update');
Route::put('/foo3/{excepciones}', [UserController::class, 'updateExcepciones'])->name('excepciones.update');
Route::put('/foo4/{periodicidad}', [UserController::class, 'updatePeriodicidad'])->name('periodicidad.update');
Route::put('/foo5/{observaciones}', [UserController::class, 'updateObservaciones'])->name('observaciones.update');
Route::put('/foo6/{consideraciones}', [UserController::class, 'updateConsideraciones'])->name('consideraciones.update');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', function () {
        return view('home');
    })->name('home');

    Route::resource('users', UserController::class)->only([
        'index', 'store', 'show', 'update', 'destroy'
    ]);

    // Index page for roles and permissions crud
    Route::get('/roles-permissions', RolePageActionController::class)->name('roles.index');

    // CRUD operations for roles and permissions
    Route::resource('roles', RoleController::class)->only([
        'store', 'show', 'update', 'destroy'
    ]);

    Route::resource('permissions', PermissionController::class)->only([
        'store', 'show', 'update', 'destroy'
    ]);

    // Index page for post categories
    // Route::get('/post-categories', PostCategoriesPageActionController::class)->name('post-categories.index');

    Route::get('/post-categories/show-options/{post_category}', [PostCategoryController::class, 'getCategories'])
    ->name('post-categories.show-options')
    ->where('PostCategory', '[0-9]+');

    Route::resource('post-categories', PostCategoryController::class)->only([
        'index', 'store', 'show',
    ]);

    Route::put('/post-categories/{id?}', [PostCategoryController::class, 'update'])->name('post-categories.update');

    // Route::get('/roles/get-users/{id}', [RoleController::class, 'getUsersByRole']);

    // Route::post('/roles/assign-role-to-users/', [RoleController::class, 'assignRoleToUsers']);

    // Route::get('/roles/get-permissions/{id}', [RoleController::class, 'getPermissionsByRole']);

    // Route::post('/roles/assign-permissions-to-role/', [RoleController::class, 'assignPermissionsToRole']);
});
