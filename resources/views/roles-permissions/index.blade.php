<x-app-layout>
    <div class="page-header">
        <div class="container-xl">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">Roles y permisos</h2>
                </div>
                <div class="col-auto ms-auto d-print-none">
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row row-cards">
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Roles</h3>
                            <div class="card-actions">
                                <a href="#" class="btn btn-primary" role="button" data-bs-toggle="modal" data-bs-target="#modal-add-role">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <line x1="12" y1="5" x2="12" y2="19" />
                                        <line x1="5" y1="12" x2="19" y2="12" />
                                    </svg>
                                    Agregar
                                </a>
                            </div>
                        </div>
                        <div class="list-group list-group-flush list-group-hoverable">

                            @foreach ($roles as $item)

                                <div class="list-group-item" data-roleid="{{$item->id}}" data-rolename="{{$item->name}}">
                                    <div class="row align-items-center">

                                        <div class="col text-truncate">
                                            {{$item->name}}
                                        </div>
                                        <div class="col-auto">
                                            <a href="#" class="list-group-item-actions" data-bs-toggle="modal" data-bs-target="#modal-edit-role" data-roleid="{{$item->id}}">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon ms-1" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1"></path><path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z"></path><path d="M16 5l3 3"></path></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            {{ $roles->appends(request()->except('roles'))->links() }}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Permisos</h3>
                            <div class="card-actions">
                                <a href="#" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modal-add-permission">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <line x1="12" y1="5" x2="12" y2="19" />
                                        <line x1="5" y1="12" x2="19" y2="12" />
                                    </svg>
                                    Agregar
                                </a>
                            </div>
                        </div>
                        <div class="list-group list-group-flush list-group-hoverable">
                            @foreach ($permissions as $item)

                                <div class="list-group-item">
                                    <div class="row align-items-center">
                                        <div class="col text-truncate">
                                            {{$item->name}}
                                        </div>
                                        <div class="col-auto">
                                            <a href="#" class="list-group-item-actions" data-bs-toggle="modal" data-bs-target="#modal-edit-permission" data-permissionId="{{$item->id}}">
                                                <svg xmlns="http://www.w3.org/2000/svg" class="icon ms-1" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1"></path><path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z"></path><path d="M16 5l3 3"></path></svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <div class="card-footer">
                            {{ $permissions->appends(request()->except('permissions'))->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Roles -->
    <div class="modal fade" id="modal-add-role" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar rol</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('roles.store') }}" method="POST" id="form-store-role">
                        @csrf

                        <div class="row">
                            <div class="col-12">
                                <div class="mb-3">
                                    <label class="form-label required" for="input-name">Nombre</label>
                                    <input type="text" class="form-control" id="input-name" name="name" />
                                    <span class="invalid-feedback d-none" id="error-name" role="alert"></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn me-auto" data-bs-dismiss="modal">Cerrar</a>
                    <button type="submit" form="form-store-role" class="btn btn-primary" onclick="!this.form && document.getElementById('form-store-role').submit()">Agregar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit-role" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Actualizar rol</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form-update-role">
                        @csrf
                        @method("PUT")

                        <div class="row">
                            <div class="col-12">
                                <div class="mb-3">
                                    <label class="form-label required" for="input-name">Nombre</label>
                                    <input type="text" class="form-control" id="input-name" name="name" />
                                    <span class="invalid-feedback d-none" id="error-name" role="alert"></span>
                                </div>
                                <input type="hidden" name="roleId" id="input-role-id">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn me-auto" data-bs-dismiss="modal">Cerrar</a>
                    <button type="submit" form="form-update-role" class="btn btn-primary" onclick="!this.form && document.getElementById('form-update-role').submit()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Permisos -->
    <div class="modal fade" id="modal-add-permission" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Agregar permiso</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('permissions.store') }}" method="POST" id="form-store-permission">
                        @csrf

                        <label class="form-label required" for="input-name">Nombre</label>
                        <input type="text" class="form-control" id="input-name" name="name" />
                        <span class="invalid-feedback d-none" id="error-name" role="alert"></span>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn me-auto" data-bs-dismiss="modal">Cerrar</a>
                    <button type="submit" form="form-store-permission" class="btn btn-primary" onclick="!this.form && document.getElementById('form-store-permission').submit()">Agregar</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-edit-permission" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Actualizar permiso</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                </div>
                <div class="modal-body">
                    <form action="" method="POST" id="form-update-permission">
                        @csrf
                        @method("PUT")

                        <label class="form-label required" for="input-name">Nombre</label>
                        <input type="text" class="form-control" id="input-name" name="name" />
                        <span class="invalid-feedback d-none" id="error-name" role="alert"></span>

                        <input type="hidden" name="permissionId" id="input-permission-id">
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn me-auto" data-bs-dismiss="modal">Cerrar</a>
                    <button type="submit" form="form-update-permission" class="btn btn-primary" onclick="!this.form && document.getElementById('form-update-permission').submit()">Guardar</button>
                </div>
            </div>
        </div>
    </div>

@push("scripts")
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        async function postData(url, data) {
            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                    },
                    body: data
                });

                const contentType = response.headers.get('content-type');

                if ( !contentType || !contentType.includes('application/json') ) {
                    if (response.status === 404) {
                        throw new Error('La acción que intentas realizar no está disponible.');
                    }

                    throw new Error("Por favor, inténtalo de nuevo más tarde.");
                }

                if (!response.ok) {
                    if ( response.status === 401)
                    {
                        throw new Error('Unauthorized');
                    }
                    else if (response.status === 403)
                    {
                        throw new Error('Forbidden');
                    }
                    else if (response.status === 404)
                    {
                        throw new Error('No se encontró el recurso que estás buscando.');
                    }
                    else if (response.status === 422)
                    {
                        console.error('Valida los campos ingresados.');
                    }
                    else
                    {
                        throw new Error('Hubo un problema al procesar la solicitud.');
                    }
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        async function getData(url) {
            try {
                const response = await fetch(url);

                const contentType = response.headers.get('content-type');

                if (!contentType || !contentType.includes('application/json')) {
                    if(response.status === 404) {
                        throw new Error('La acción que intentas realizar no está disponible');
                    }

                    throw new Error("Por favor, inténtalo de nuevo más tarde.");
                }

                if (!response.ok) {
                    throw new Error("Hubo un problema al obtener el recurso.");
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        // Roles
        const modalAddRole = document.getElementById("modal-add-role");

        const formStoreRole = document.getElementById("form-store-role");

        formStoreRole.addEventListener("submit", function(event) {
            event.preventDefault();

            const formData = new FormData(this);

            postData('/roles', formData)
            .then(result => {
                if(result.errors)
                {
                    clearInputErrors(formStoreRole);
                    fillInputErrors(formStoreRole, result.errors);
                }
                else
                {
                    successAlertWithReload(result.message);
                    const myModal = bootstrap.Modal.getOrCreateInstance(modalAddRole);
                    myModal.hide();
                }
            })
            .catch(error => errorAlert(error.message));
        });

        modalAddRole.addEventListener("hidden.bs.modal", function (event) {
            clearInputErrors(formStoreRole);
            clearFields(formStoreRole);
        });

        const modalEditRole = document.getElementById("modal-edit-role");

        const formUpdateRole = document.getElementById("form-update-role");

        modalEditRole.addEventListener("show.bs.modal", function (event) {
            const modal = this;

            const button = event.relatedTarget;

            const roleId = button.getAttribute("data-roleId");

            if( roleId && !isNaN(roleId) && Number.isInteger(parseFloat(roleId)) ) {
                const url = `/roles/${roleId}`;

                getData(url)
                .then(result => {
                        const inputRoleId = modal.querySelector("#input-role-id");
                        const inputName = modal.querySelector("#input-name");

                        inputRoleId.value = result.id;
                        inputName.value  = result.name;
                })
                .catch(error => errorAlert(error.message));
            }
        });

        formUpdateRole.addEventListener("submit", function(event) {
            event.preventDefault();

            const formData = new FormData(this);

            const roleId = formData.get("roleId");

            if( roleId && !isNaN(roleId) && Number.isInteger(parseFloat(roleId)) ) {

                const url = `/roles/${roleId}`;

                postData(url, formData)
                .then(result => {
                    if(result.errors) {
                        clearInputErrors(formUpdateRole);
                        fillInputErrors(formUpdateRole, result.errors);
                    }
                    else {
                        successAlertWithReload(result.message);
                        const myModal = bootstrap.Modal.getOrCreateInstance(modalEditRole);
                        myModal.hide();
                    }
                })
                .catch(error => errorAlert(error.message));
            }
        });

        modalEditRole.addEventListener("hidden.bs.modal", function (event) {
            clearInputErrors(formUpdateRole);
            clearFields(formUpdateRole);
        });

        // Permisos
        const modalAddPermission =  document.getElementById("modal-add-permission");

        const formStorePermission = document.getElementById("form-store-permission");

        formStorePermission.addEventListener("submit", function(event) {
            event.preventDefault();

            const formData = new FormData(this);
            postData('/permissions', formData)
            .then(result => {
                if(result.errors)
                {
                    clearInputErrors(formStorePermission);
                    fillInputErrors(formStorePermission, result.errors);
                }
                else
                {
                    successAlertWithReload(result.message);
                    const myModal = bootstrap.Modal.getOrCreateInstance(modalAddPermission);
                    myModal.hide();
                }
            })
            .catch(error => errorAlert(error.message));
        });

        modalAddPermission.addEventListener("hidden.bs.modal", function (event) {
            clearInputErrors(formStorePermission);
            clearFields(formStorePermission);
        });

        const modalEditPermission = document.getElementById("modal-edit-permission");

        const formUpdatePermission = document.getElementById("form-update-permission");

        modalEditPermission.addEventListener("show.bs.modal", function (event) {
            const modal = this;

            const button = event.relatedTarget;

            const permissionId = button.getAttribute("data-permissionId");

            if( permissionId && !isNaN(permissionId) && Number.isInteger(parseFloat(permissionId)) ) {
                const url = `/permissions/${permissionId}`;
                getData(url)
                .then(result => {
                    const inputPermissionId = modal.querySelector("#input-permission-id");
                    const inputName  = modal.querySelector("#input-name");

                    inputPermissionId.value = result.id;
                    inputName.value  = result.name;
                })
                .catch(error => errorAlert(error.message));
            }
        });

        formUpdatePermission.addEventListener("submit", function(event) {
            event.preventDefault();

            const formData = new FormData(this);

            const permissionId = formData.get("permissionId");

            if ( permissionId && !isNaN(permissionId) && Number.isInteger(parseFloat(permissionId)) ) {

                const url = `/permissions/${permissionId}`;

                postData(url, formData)
                .then(result => {
                    if(result.errors) {
                        clearInputErrors(formUpdatePermission);
                        fillInputErrors(formUpdatePermission, result.errors);
                    }
                    else {
                        successAlertWithReload(result.message);
                        const myModal = bootstrap.Modal.getOrCreateInstance(modalEditPermission);
                        myModal.hide();
                    }
                })
                .catch(error => errorAlert(error.message));
            }
        });

        modalEditPermission.addEventListener("hidden.bs.modal", function (event) {
            clearInputErrors(formUpdatePermission);
            clearFields(formUpdatePermission);
        });

        function fillInputErrors(form, errors) {
            Object.keys(errors).forEach(campo => {
                const inputElement = form.querySelector(`[name="${campo}"]`);
                // Verificar si hay errores para este campo en el JSON
                if (errors[campo] && inputElement) {

                    inputElement.classList.add("is-invalid");

                    const spanFeedback = form.querySelector(`#error-${campo}`);

                    const error = errors[campo][0];

                    spanFeedback.classList.remove("d-none");
                    spanFeedback.textContent = error;
                }
            });
        }

        function clearFields(form) {
            // const inputs = form.querySelectorAll(".form-control, .form-select");
            const inputs = form.getElementsByClassName("form-control");

            for (const key in inputs) {
                inputs[key].value = "";
            }
        }

        function clearInputErrors(form) {
            inputs = form.querySelectorAll("input:not([type='hidden'])");
            // inputs = form.querySelectorAll("input:not([type='hidden']):not([type='checkbox'])");

            for (const input of inputs) {
                const span = input.nextElementSibling;
                if(span){
                    input.classList.remove("is-invalid");
                    span.classList.add("d-none");
                    span.textContent = "";
                }
            }
        }

        function successAlertWithReload(message) {
            Swal.fire({
                title: "¡Perfecto!",
                text: message,
                icon: "success",
                showConfirmButton: false,
                timer: 2500
            }).then(() => {
                location.reload();
            });
        }

        function errorAlert(message) {
            Swal.fire('¡Algo salió mal!', message, 'error');
        }
    </script>
@endpush
</x-app-layout>
