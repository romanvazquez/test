<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Registro de usuarios</title>
    <link rel="stylesheet" href="{{ asset('css/tabler-new.min.css') }}">

    <style>
        .btn-c1-light {
            color: #ab0033;
            border-color: #ab0033;
            border-radius: 15px;
            background: white;
            cursor: pointer;
        }

        .btn-c1-light:hover {
            color: white !important;
            border-color: #ab0033;
            background: #ab0033;
        }

        .btn-c2-light {
            color: #bc955c;
            border-color: #bc955c;
            border-radius: 15px;
            background: white;
            cursor: pointer;
        }

        .btn-c2-light:hover {
            color: white !important;
            border-color: #bc955c;
            background: #bc955c;
        }

        .btn-c3-light {
            cursor: pointer;
            background: white;
            color: #ddc9a3;
            border-radius: 15px;
            border-color: #ddc9a3;
        }

        .btn-c3-light:hover {
            color: white !important;
            border-color: #ddc9a3;
            background: #ddc9a3;
        }

        .btn-c4-light {
            cursor: pointer;
            background: white;
            color: #54565a;
            border-color: #54565a;
        }

        .btn-c4-light:hover {
            color: white !important;
            border-color: #54565a;
            background: #54565a;
        }

        .img-responsive.img-responsive-21x9.card-img-top::before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: radial-gradient(rgba(204, 63, 105, 0.5), rgba(171, 0, 51, 0.5));
            /* Ajusta los colores y la opacidad según sea necesario */
            z-index: 1;
            pointer-events: none;
        }
    </style>
</head>
<body class="d-flex flex-column">
    <div class="page page-center">
        <div class="container container-tight py-4">
            <!-- <div class="text-center mb-4">
                <a href="#" class="navbar-brand navbar-brand-autodark">
                    <img src="" height="36" alt="">
                </a>
            </div> -->

            <div class="card card-md">
                {{-- <div class="img-responsive img-responsive-21x9 card-img-top" style="position: relative; background-image: url({{asset('img/auth-one-bg.jpg')}})"></div> --}}

                <!-- <div class="img-responsive img-responsive-21x9 card-img-top" style="position: relative; background-image: url('img/secude.jpg');"></div> -->

                <div class="card-body">

                    <div class="alert alert-danger" role="alert" id="alerta-error" style="display: none;">
                        <div class="d-flex">
                            <div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon alert-icon"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M3 12a9 9 0 1 0 18 0a9 9 0 0 0 -18 0"></path><path d="M12 8v4"></path><path d="M12 16h.01"></path></svg>
                            </div>
                            <div id="alerta-contenido">
                            </div>
                        </div>
                    </div>

                    <p class="tex-secondary">
                        1. Ingresa tu <em>RFC</em> en el campo de texto siguiente y luego da click en el botón que dice <strong>Buscar</strong>, junto al campo de texto.</li>
                    </p>

                    <form id="form-buscar-empleado" action="">
                        <div class="row mb-2">
                            <div class="col">
                                <input type="text" class="form-control" id="rfc-input" name="rfc" pattern="[A-Za-z]{4}\d{6}[A-Za-z0-9]{0,3}" minlength="10" maxlength="13" placeholder="Ingresa tu RFC" required>
                            </div>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-c2-light" id="btn-buscar">
                                    <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M10 10m-7 0a7 7 0 1 0 14 0a7 7 0 1 0 -14 0"></path><path d="M21 21l-6 -6"></path></svg>
                                    Buscar
                                </button>
                            </div>
                        </div>
                        <div class="form-hint d-none" id="hint-cambiar-rfc">
                            ¿No es tu RFC?
                            <span class="form-label-description">
                                <a href="#" onclick="deshabilitarSignUp()">Haz click aquí para cambiarlo</a>
                            </span>
                        </div>
                    </form>
                </div>

                <div class="hr-text hr-text-center hr-text-spaceless d-none" id="segunda-seccion">Crear cuenta</div>

                <div class="card-body d-none" id="segundo-formulario">

                    <p class="tex-secondary">
                        2. Para continuar con el registro, debes ingresar los siguientes datos:
                    </p>
                    <ul>
                        <li>Dirección de correo electrónico</li>
                        <li>Contraseña que sea fácil de recordar o que puedas guardar en un lugar seguro</li>

                    </ul>

                    <form action="" method="post" id="form-registrar-usuario">
                        @csrf

                        <div class="mb-3">
                            <label class="form-label" for="email-input">Correo electrónico</label>
                            <input type="text" id="email-input" name="email" class="form-control" autofocus>
                            <span class="invalid-feedback" id="email-error" role="alert"></span>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="password-input">Contraseña</label>

                            <div class="row g-2">
                                <div class="col">
                                    {{-- <input type="password" id="password-input" name="password" class="form-control @error('password') is-invalid @enderror" autocomplete="off" required> --}}
                                    <input type="password" id="password-input" name="password" class="form-control" autocomplete="off">
                                    <span class="invalid-feedback" id="password-error" role="alert"></span>
                                </div>

                                <div class="col-auto">
                                    <button type="button" class="btn btn-c3-light btn-icon" onclick="showPassword()" title="Mostrar" data-bs-toggle="tooltip" tabindex="-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24"
                                            height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                            fill="none" stroke-linecap="round" stroke-linejoin="round">
                                            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                            <circle cx="12" cy="12" r="2" />
                                            <path
                                                d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="password-confirmation-input">Confirmación de la contraseña</label>
                            {{-- <input type="password" id="password-confirmation-input" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" value="{{ old('password_confirmation') }}" required> --}}
                            <input type="password" id="password-confirmation-input" name="password_confirmation" class="form-control">
                        </div>

                        <input type="hidden" id="ta-empleado-id-input" name="ta_empleado_id" class="form-control">

                        <div class="form-footer">
                            <button type="submit" class="btn btn-c1-light w-100" id="btn-registrar">Registrarme</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        async function postData(url, data) {
            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                    },
                    body: data
                });

                if (!response.ok) {
                    if (response.status === 404)
                    {
                        throw new Error('No se encontró el recurso que estás buscando.');
                    }
                    else if (response.status === 422)
                    {
                        console.error('Valida los campos ingresados.');
                    }
                    else
                    {
                        throw new Error('Hubo un problema al procesar la solicitud.');
                    }
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        async function getData(url) {
            try {
                const response = await fetch(url);

                if (!response.ok) {
                    if (response.status === 404)
                    {
                        throw new Error('No se encontró el recurso que estás buscando.');
                    }
                    else if (response.status === 422)
                    {
                        console.error('Valida los campos ingresados.');
                    }
                    else
                    {
                        throw new Error('Hubo un problema al procesar la solicitud.');
                    }
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        const inputRfc = document.getElementById("rfc-input");

        const btnBuscar = document.getElementById("btn-buscar");

        const hintCambiarRfc = document.getElementById("hint-cambiar-rfc");

        const alertaError = document.getElementById("alerta-error");

        const alertaContenido = document.getElementById("alerta-contenido");

        // Segunda sección
        const segundaSeccion = document.getElementById('segunda-seccion');
        const segundoFormulario = document.getElementById('segundo-formulario');

        const inputEmail = document.getElementById("email-input");
        const inputPassword = document.getElementById("password-input");
        const inputPasswordConfirmation = document.getElementById("password-confirmation-input");
        const inputTaEmpleadoId = document.getElementById("ta-empleado-id-input");

        const btnRegistrar = document.getElementById("btn-registrar")

        // Función para validar el RFC
        function validarRfc(rfc) {
            const pattern = /^[A-Za-z]{4}\d{6}[A-Za-z0-9]{0,3}$/;
            return pattern.test(rfc);
        }

        function errorAlert(message) {
            Swal.fire('¡Algo salió mal!', message, 'error');
        }

        function loadingAlert() {
            Swal.fire({
                title: 'Cargando...',
                text: 'Por favor, espera un momento.',
                allowOutsideClick: false,
                didOpen: () => {
                    Swal.showLoading();
                }
            });
        }

        function fadeIn(element, duration) {
            let opacity = 0;
            const interval = 50;
            const increment = interval / duration;

            function animate() {
                opacity += increment;
                if (opacity <= 1) {
                    element.style.opacity = opacity;
                    requestAnimationFrame(animate);
                }
            }

            animate();
        }

        function fadeOut(element, duration, callback) {
            let opacity = 1;
            const interval = 50;
            const decrement = interval / duration;

            function animate() {
                opacity -= decrement;
                if (opacity >= 0) {
                    element.style.opacity = opacity;
                    requestAnimationFrame(animate);
                } else {
                    if (callback) callback();
                }
            }

            animate();
        }

        function deshabilitarSignUp() {
            fadeOut(segundaSeccion, 800, function() {
                segundaSeccion.classList.add('d-none');
            });

            fadeOut(segundoFormulario, 800, function() {
                segundoFormulario.classList.add('d-none');
            });

            fadeOut(hintCambiarRfc, 800, function() {
                hintCambiarRfc.classList.add('d-none');
            });

            // Volver a habilitar el campo RFC y el botón de submit
            inputRfc.readOnly = false;

            // Remover la apariencia de un campo de texto deshabilitado
            inputRfc.classList.remove("disabled");

            // Habilitar eventos de puntero para que no se pueda interactuar con el campo
            inputRfc.style.pointerEvents = 'auto';

            // Incluye el campo en el orden de tabulación
            inputRfc.tabIndex = 0;

            btnBuscar.disabled = false;

            // Limpiar y deshabilitar los campos del registro de usuarios
            inputEmail.value = "";
            inputPassword.value = "";
            inputPasswordConfirmation.value = "";
            inputTaEmpleadoId.value = "";

            inputEmail.disabled = true;
            inputPassword.disabled = true;
            inputPasswordConfirmation.disabled = true;
            inputTaEmpleadoId.disabled = true;

            btnRegistrar.disabled = true;
        }

        // Habilita la segunda sección del registro (segundo formulario) y, asimismo, inactiva las modificaciones para el primer formulario
        function habilitarSignUp() {
            // Mostrar la segunda sección con una transición suave
            segundaSeccion.classList.remove('d-none');
                segundoFormulario.classList.remove('d-none');

                hintCambiarRfc.classList.remove('d-none');

                segundaSeccion.style.opacity = 0;
                segundoFormulario.style.opacity = 0;

                hintCambiarRfc.style.opacity = 0;

                // Aplicar la animación de aumento de opacidad
                fadeIn(segundaSeccion, 800);
                fadeIn(segundoFormulario, 800);

                fadeIn(hintCambiarRfc, 800);

                // Deshabilitar el campo RFC y el botón de submit
                // Establece el campo como solo lectura
                inputRfc.readOnly = true;

                // Darle la apariencia de un campo de texto deshabilitado
                inputRfc.classList.add("disabled");

                // Deshabilita eventos de puntero para que no se pueda interactuar con el campo
                inputRfc.style.pointerEvents = 'none';

                // Elimina el campo del orden de tabulación
                inputRfc.tabIndex = -1;

                btnBuscar.disabled = true;

                // Habilitar los campos del registro de usuarios
                inputEmail.disabled = false;
                inputPassword.disabled = false;
                inputPasswordConfirmation.disabled = false;
                inputTaEmpleadoId.disabled = false;

                btnRegistrar.disabled = false;
        }

        // Limpiar contenido de la alerta y ocultarla
        function clearAlertError() {
            // Limpiar el contenido del cuerpo de la alerta
            alertaContenido.innerHTML = "";

            // Ocultar la alerta
            alertaError.style.display = "none";
        }

        // Llenar contenido de la alerta y mostrarla
        function fillAlertError(mensaje) {
            // Llenar el cuerpo de la alerta con el mensaje de error
            alertaContenido.innerHTML = mensaje;

            // Mostrar la alerta
            alertaError.style.display = "block";
        }


        document.addEventListener("DOMContentLoaded", function() {
            const formBuscarEmpleado = document.getElementById("form-buscar-empleado");

            const formRegistrarUsuario = document.getElementById("form-registrar-usuario");

            formBuscarEmpleado.addEventListener("submit", function(event) {
                event.preventDefault();

                let rfc = inputRfc.value;

                // MOSTRAR VENTANA DE ESPERA
                if (rfc) {
                    if(!validarRfc(rfc)){
                        fillAlertError("El RFC ingresado no es válido.");
                        return;
                    }

                    const url = `https://localhost:7120/RegistrarUsuario/${rfc}`;

                    getData(url)
                    .then(result => {

                        console.log(result);

                        if(result.log && !result.log.flag) {
                            fillAlertError(result.log.mensaje);
                            deshabilitarSignUp();
                        }
                        else {
                            if(result.empleado){
                                let id = result.empleado.ta_empleado_Id;
                                rfc = result.empleado.rfc;

                                if(rfc && !isNaN(id) && Number.isInteger(id)){
                                    inputTaEmpleadoId.value = id;
                                    inputRfc.value = rfc

                                    clearAlertError();
                                    habilitarSignUp();
                                }
                            }
                        }
                    })
                    .catch(error => errorAlert(error.message));
                } else {
                    fillAlertError("Por favor, ingrese su RFC.");
                }
            });

            formRegistrarUsuario.addEventListener("submit", function(event) {
                event.preventDefault();

                const rfc = inputRfc.value;

                // MOSTRAR VENTANA DE ESPERA
                if (rfc) {
                    if(!validarRfc(rfc)){
                        fillAlertError("El RFC ingresado no es válido.");
                        deshabilitarSignUp();
                    }

                    const formData = new FormData(this);

                    formData.append('rfc', rfc);

                    console.log( Object.fromEntries(formData) );

                    postData('https://localhost:7120/RegistrarUsuario/', formData)
                    .then(result => {

                        clearInputErrors(formRegistrarUsuario);

                        if(result.errors)
                        {
                            fillInputErrors(result.errors);
                        }
                        else
                        {
                            if(result.log && !result.log.flag) {
                                fillAlertError(result.log.mensaje);
                            }


                            // successAlert(result.message);
                            // const myModal = bootstrap.Modal.getOrCreateInstance(modalAddUser);
                            // myModal.hide();
                        }
                    })
                    .catch(error => errorAlert(error.message));

                } else {

                    fillAlertError("Por favor, ingrese su RFC.");
                    deshabilitarSignUp();

                }
            });

            // function fillInputErrors(form, errors) {
            function fillInputErrors(errors) {

                errors.forEach(function(elemento) {
                    document.getElementById((elemento.key).toLowerCase()+'-input').classList.add('is-invalid');
                    // document.getElementById((elemento.key).toLowerCase()+'-error').classList.remove('d-none');
                    document.getElementById((elemento.key).toLowerCase()+'-error').textContent = elemento.errors[0].errorMessage;
                });
            }

            function clearInputErrors(form) {
                inputs = form.querySelectorAll("input:not([type='hidden'])");

                for (const input of inputs) {
                    const span = input.nextElementSibling;
                    if(span){
                        input.classList.remove("is-invalid");
                        // span.classList.add("d-none");
                        span.textContent = "";
                    }
                }
            }
        });
    </script>
</body>
</html>
