<x-app-layout>
    @push('styles')
        <style>
            /* Estilo adicional para resaltar la fila seleccionada */
            /* Estilo para la fila seleccionada */
            .table-hover tbody tr.seleccionado {
                background-color: #007bff;
                color: #ffffff;
            }
        </style>
    @endpush

    <div class="page-header">
        <div class="container-xl">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">Usuarios</h2>
                </div>
                <div class="col-auto ms-auto d-print-none">
                    <div class="btn-list">
                        <!-- Agregar -->
                        <a href="#" class="btn btn-primary d-none d-sm-inline-block" data-bs-toggle="modal" data-bs-target="#modal-add-user">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                <line x1="12" y1="5" x2="12" y2="19" />
                                <line x1="5" y1="12" x2="19" y2="12" />
                            </svg>
                            Agregar
                        </a>
                        <a href="#" class="btn btn-primary d-sm-none btn-icon" aria-label="" data-bs-toggle="modal" data-bs-target="#modal-add-user">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                <line x1="12" y1="5" x2="12" y2="19" />
                                <line x1="5" y1="12" x2="19" y2="12" />
                            </svg>
                        </a>

                        <!-- Actualizar -->
                        <a href="#" class="btn btn-light d-none d-sm-inline-block btnEdit disabled" data-bs-toggle="modal" data-bs-target="#modal-edit-user">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                <path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                <path d="M16 5l3 3" />
                            </svg>
                            Actualizar
                        </a>
                        <a href="#" class="btn btn-light d-sm-none btn-icon btnEdit disabled" aria-label="" data-bs-toggle="modal" data-bs-target="#modal-edit-user">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                <path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                <path d="M16 5l3 3" />
                            </svg>
                        </a>

                        <!-- Eliminar -->
                        <a href="#" role="button" class="btn btn-danger d-none d-sm-inline-block btnDelete disabled" onclick="confirmDelete()">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon"
                                width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                stroke="currentColor" fill="none" stroke-linecap="round"
                                stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M4 7l16 0"></path>
                                <path d="M10 11l0 6"></path>
                                <path d="M14 11l0 6"></path>
                                <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                            </svg>
                            Eliminar
                        </a>
                        <a href="#" class="btn btn-danger d-sm-none btn-icon btnDelete disabled" aria-label="">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon"
                                width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                stroke="currentColor" fill="none" stroke-linecap="round"
                                stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M4 7l16 0"></path>
                                <path d="M10 11l0 6"></path>
                                <path d="M14 11l0 6"></path>
                                <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <!--
                            Pendiente meter el siguiente bloque en un formulario
                            El evento de cambio al seleccionar una opción debe hacer submit al formulario en
                        -->
                        <div class="card-body border-bottom py-3">
                            <form action="{{ route('users.index') }}" method="get" id="form-search">
                                <div class="d-flex">
                                    <div class="text-secondary">
                                        Mostrar
                                        <div class="mx-1 mx-lg-0 mx-xl-2 d-inline-block">
                                            <select name="perPage" class="form-select" aria-label="Cantidad de usuarios por página" onchange="this.form.submit()">
                                                <option value="5" {{ request('perPage') == 5 ? 'selected' : '' }}>5</option>
                                                <option value="10" {{ request('perPage') == 10 ? 'selected' : '' }}>10</option>
                                                <option value="15" {{ request('perPage') == 15 ? 'selected' : '' }}>15</option>
                                                <option value="20" {{ request('perPage') == 20 ? 'selected' : '' }}>20</option>
                                            </select>
                                        </div>
                                        resultados
                                    </div>
                                    <!-- <div class="ms-auto">
                                        <div class="row g-2">
                                            <div class="col">
                                                <input type="search" class="form-control" name="search" value="{{ request('search') }}" placeholder="Buscar">
                                            </div>
                                            <div class="col-auto">
                                                <button type="submit" form="form-store-user" class="btn btn-icon" onclick="this.form.submit()">
                                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M10 10m-7 0a7 7 0 1 0 14 0a7 7 0 1 0 -14 0"></path><path d="M21 21l-6 -6"></path></svg>
                                                </button>
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                            </form>
                        </div>
                        <table class="table card-table table-vcenter table-hover table-mobile-md" id="tblUsuarios">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Correo electrónico</th>
                                    <th>Estatus</th>
                                    <th>Fecha de creación</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $item)

                                <tr id="{{$item->id}}">
                                    <td>
                                        {{$item->nombre_completo}}
                                    </td>
                                    <td>
                                        {{$item->email}}
                                    </td>
                                    <td>
                                        {{$item->is_active ? "Activo" : "Inactivo"}}
                                    </td>
                                    <td>
                                        {{$item->created_at}}
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <!-- <div class="card-footer ">
                            <ul class="pagination">
                                <li class="page-item disabled">
                                    <a class="page-link" href="#" tabindex="-1" aria-disabled="true">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M15 6l-6 6l6 6"></path></svg>
                                        prev
                                    </a>
                                </li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">5</a></li>
                                <li class="page-item">
                                    <a class="page-link" href="#">
                                        next
                                        <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M9 6l6 6l-6 6"></path></svg>
                                    </a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>



    </div>

@push("scripts")
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        async function postData(url, data) {
            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                    },
                    body: data
                });

                const contentType = response.headers.get('content-type');

                if ( !contentType || !contentType.includes('application/json') ) {
                    if (response.status === 404) {
                        throw new Error('No está disponible la acción que intentas realizar.');
                    }

                    throw new Error("Por favor, inténtalo de nuevo más tarde.");
                }

                if (!response.ok) {
                    if ( response.status === 401)
                    {
                        throw new Error('Unauthorized');
                    }
                    else if (response.status === 403)
                    {
                        throw new Error('Forbidden');
                    }
                    else if (response.status === 404)
                    {
                        throw new Error('No se encontró el recurso que estás buscando.');
                    }
                    else if (response.status === 422)
                    {
                        console.error('Valida los campos ingresados.');
                    }
                    else
                    {
                        throw new Error('Hubo un problema al procesar la solicitud.');
                    }
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        async function getData(url) {
            try {
                const response = await fetch(url);

                const contentType = response.headers.get('content-type');

                if (!contentType || !contentType.includes('application/json')) {
                    if(response.status === 404) {
                        throw new Error('No está disponible la acción que intentas realizar.');
                    }

                    throw new Error("Por favor, inténtalo de nuevo más tarde.");
                }

                if (!response.ok) {
                    throw new Error("Hubo un problema al obtener el recurso.");
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        function confirmDelete() {
            const button = event.currentTarget;

            const id = button.getAttribute("data-id");

            if ( id && !isNaN(id) && Number.isInteger(parseFloat(id)) ) {
                Swal.fire({
                    title: '¿Estás seguro?',
                    text: "No podrás revertir esta acción",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        deleteRecord(id);
                    }
                });
            }
        }

        function deleteRecord(id) {
            fetch(`/users/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                }
            })
            .then(response => {

                const contentType = response.headers.get('content-type');

                if ( !contentType || !contentType.includes('application/json') ) {
                    if (response.status === 404) {
                        throw new Error('No está disponible la acción que intentas realizar.');
                    }

                    throw new Error("Por favor, inténtalo de nuevo más tarde.");
                }

                if (!response.ok) {
                    throw new Error('Hubo un problema al eliminar el registro.');
                }

                return response.json();
            })
            .then(result => {
                successAlert(result.message);
            })
            .catch(error => errorAlert(error.message) );
        }

        const tblUsuarios = document.getElementById("tblUsuarios").tBodies[0];

        // Botones de acciones
        const btnEdit = document.querySelectorAll('.btnEdit');
        const btnDelete = document.querySelectorAll('.btnDelete');

        tblUsuarios.addEventListener("click", function(event) {
            const row = event.target.closest("tr");

            // Verificar si se hizo clic en una fila y no en otro elemento dentro de la tabla
            if (row && tblUsuarios.contains(row)) {

                // Si la fila ya está seleccionada, desmarcarla y deshabilitar el botón
                if (row.classList.contains("seleccionado")) {

                    row.classList.remove("seleccionado");

                    // Para deshabilitar los botones
                    disableButtons(btnEdit);
                    disableButtons(btnDelete);
                } else {
                    // Eliminar la clase "seleccionado" de todas las filas
                    const rows = tblUsuarios.getElementsByTagName("tr");

                    for (let i = 0; i < rows.length; i++) {
                        rows[i].classList.remove("seleccionado");
                    }

                    // Agregar la clase "seleccionado" a la fila que selecciona
                    row.classList.add("seleccionado");

                    // Para habilitar los botones y asignarles un id al seleccionar una fila
                    enableButtons(btnEdit, row.id);
                    enableButtons(btnDelete, row.id);
                }

            } else {
                // SI se hizo clic fuera de una fila, ENTONCES deshabilitar los botones
                disableButtons(btnEdit);
                disableButtons(btnDelete);
            }
        });

        function disableButtons(buttons) {
            buttons.forEach(button => {
                button.classList.add("disabled");
                button.removeAttribute("data-id");
            });
        }

        function enableButtons(buttons, id) {
            buttons.forEach(button => {
                button.classList.remove("disabled");
                button.setAttribute("data-id", id);
            });
        }

        disableButtons(btnEdit);
        disableButtons(btnDelete);

        const modalAddUser =  document.getElementById("modal-add-user");

        const formStoreUser = document.getElementById("form-store-user");

        formStoreUser.addEventListener("submit", function(event) {
            event.preventDefault();

            const formData = new FormData(this);
            postData('/users', formData)
            .then(result => {
                if(result.errors)
                {
                    clearInputErrors(formStoreUser);
                    fillInputErrors(formStoreUser, result.errors);
                }
                else
                {
                    successAlert(result.message);
                    const myModal = bootstrap.Modal.getOrCreateInstance(modalAddUser);
                    myModal.hide();
                }
            })
            .catch(error => errorAlert(error.message));
        });

        modalAddUser.addEventListener("hidden.bs.modal", function (event) {
            clearInputErrors(formStoreUser);
            clearFields(formStoreUser);
        });

        const modalEditUser = document.getElementById("modal-edit-user");

        const formUpdateUser = document.getElementById("form-update-user");

        modalEditUser.addEventListener("show.bs.modal", function (event) {
            const modal = this;

            const button = event.relatedTarget;

            const id = button.getAttribute("data-id");

            if( id && !isNaN(id) && Number.isInteger(parseFloat(id)) ) {
                const url = `/users/${id}`;

                getData(url)
                .then(result => {
                    const id = modal.querySelector("#input-id");
                    const inputNombre  = modal.querySelector("#input-nombre");
                    const inputPaterno = modal.querySelector("#input-paterno");
                    const inputMaterno = modal.querySelector("#input-materno");
                    const inputEmail = modal.querySelector("#input-email");
                    const inputUsername = modal.querySelector("#input-username");

                    id.value = result.id;
                    inputNombre.value  = result.nombre;
                    inputPaterno.value = result.paterno;
                    inputMaterno.value = result.materno;
                    inputEmail.value = result.email;
                    inputUsername.value = result.username;
                })
                .catch(error => errorAlert(error.message));
            }
        });

        formUpdateUser.addEventListener("submit", function(event) {
            event.preventDefault();

            const formData = new FormData(this);

            const id = formData.get("id");

            if ( id && !isNaN(id) && Number.isInteger(parseFloat(id)) ) {

                const url = `/users/${id}`;

                postData(url, formData)
                .then(result => {
                    if(result.errors) {
                        clearInputErrors(formUpdateUser);
                        fillInputErrors(formUpdateUser, result.errors);
                    }
                    else {
                        successAlert(result.message);
                        const myModal = bootstrap.Modal.getOrCreateInstance(modalEditUser);
                        myModal.hide();
                    }
                })
                .catch(error => errorAlert(error.message));
            }
        });

        modalEditUser.addEventListener("hidden.bs.modal", function (event) {
            clearInputErrors(formUpdateUser);
            clearFields(formUpdateUser);
        });

        function fillInputErrors(form, errors) {
            Object.keys(errors).forEach(campo => {
                const inputElement = form.querySelector(`[name="${campo}"]`);
                // Verificar si hay errores para este campo en el JSON
                if (errors[campo] && inputElement) {

                    inputElement.classList.add("is-invalid");

                    const spanFeedback = form.querySelector(`#error-${campo}`);

                    const error = errors[campo][0];

                    spanFeedback.classList.remove("d-none");
                    spanFeedback.textContent = error;
                }
            });
        }

        function clearFields(form) {
            inputs = form.getElementsByClassName("form-control");

            for (const key in inputs) {
                inputs[key].value = "";
            }
        }

        function clearInputErrors(form) {
            inputs = form.querySelectorAll("input:not([type='hidden'])");

            for (const input of inputs) {
                const span = input.nextElementSibling;
                if(span){
                    input.classList.remove("is-invalid");
                    span.classList.add("d-none");
                    span.textContent = "";
                }
            }
        }

        function successAlert(message) {
            Swal.fire({
                title: "¡Perfecto!",
                text: message,
                icon: "success",
                showConfirmButton: false,
                timer: 3000
            }).then(() => {
                location.reload();
            });
        }

        function errorAlert(message) {
            Swal.fire('¡Algo salió mal!', message, 'error');
        }
    </script>
@endpush
</x-app-layout>
