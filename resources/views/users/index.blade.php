<x-app-layout>
    <div class="page-header">
        <div class="container-xl">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">Usuarios</h2>
                </div>
                <div class="col-auto ms-auto d-print-none">
                    <div class="btn-list">
                        <!-- Agregar -->
                        <a href="#" class="btn btn-primary d-none d-md-inline-block" data-bs-toggle="modal" data-bs-target="#modal-add-user">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                <line x1="12" y1="5" x2="12" y2="19" />
                                <line x1="5" y1="12" x2="19" y2="12" />
                            </svg>
                            Agregar
                        </a>
                        <a href="#" class="btn btn-primary d-md-none btn-icon" aria-label="" data-bs-toggle="modal" data-bs-target="#modal-add-user">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                <line x1="12" y1="5" x2="12" y2="19" />
                                <line x1="5" y1="12" x2="19" y2="12" />
                            </svg>
                        </a>

                        <!-- Actualizar -->
                        <a href="#" class="btn btn-secondary d-none d-md-inline-block btnEdit disabled" data-bs-toggle="modal" data-bs-target="#modal-edit-user">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                <path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                <path d="M16 5l3 3" />
                            </svg>
                            Actualizar
                        </a>
                        <a href="#" class="btn btn-secondary d-md-none btn-icon btnEdit disabled" aria-label="" data-bs-toggle="modal" data-bs-target="#modal-edit-user">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                stroke-linecap="round" stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                <path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" />
                                <path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" />
                                <path d="M16 5l3 3" />
                            </svg>
                        </a>

                        <!-- Eliminar -->
                        <a href="#" role="button" class="btn btn-danger d-none d-md-inline-block btnDelete disabled" onclick="confirmDelete()">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon"
                                width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                stroke="currentColor" fill="none" stroke-linecap="round"
                                stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M4 7l16 0"></path>
                                <path d="M10 11l0 6"></path>
                                <path d="M14 11l0 6"></path>
                                <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                            </svg>
                            Eliminar
                        </a>
                        <a href="#" role="button" class="btn btn-danger d-md-none btn-icon btnDelete disabled" onclick="confirmDelete()" aria-label="">
                            <svg xmlns="http://www.w3.org/2000/svg" class="icon"
                                width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                stroke="currentColor" fill="none" stroke-linecap="round"
                                stroke-linejoin="round">
                                <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                <path d="M4 7l16 0"></path>
                                <path d="M10 11l0 6"></path>
                                <path d="M14 11l0 6"></path>
                                <path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12"></path>
                                <path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3"></path>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Page body -->
    <div class="page-body">
        <div class="container-xl">
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <table class="table card-table table-vcenter table-hover table-mobile-md" id="tblUsuarios">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Correo electrónico</th>
                                    <th>Estatus</th>
                                    <th>Fecha de creación</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $item)

                                <tr id="{{$item->id}}">
                                    <td data-label="Nombre">
                                        {{$item->nombre_completo}}
                                    </td>
                                    <td data-label="Email">
                                        {{$item->email}}
                                    </td>
                                    <td data-label="Estatus">
                                        {{$item->is_active ? "Activo" : "Inactivo"}}
                                    </td>
                                    <td data-label="Fecha de creación">
                                        {{$item->created_at}}
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('modals')

        <div class="modal fade" id="modal-add-user" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Agregar usuario</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('users.store')}}" method="POST" id="form-store-user">
                            @csrf

                            <div class="row">
                                <div class="col-12 mb-3">
                                    <label class="form-label required" for="input-nombre">Nombre</label>
                                    <input type="text" class="form-control" id="input-nombre" name="nombre" />
                                    <span class="invalid-feedback d-none" id="error-nombre" role="alert"></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label class="form-label" for="input-paterno">Apellido paterno</label>
                                    <input type="text" class="form-control" id="input-paterno" name="paterno" />
                                    <span class="invalid-feedback d-none" id="error-paterno" role="alert"></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label class="form-label" for="input-materno">Apellido materno</label>
                                    <input type="text" class="form-control" id="input-materno" name="materno" />
                                    <span class="invalid-feedback d-none" id="error-materno" role="alert"></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label class="form-label required" for="input-email">Correo electrónico</label>
                                    <input type="text" class="form-control" id="input-email" name="email" />
                                    <span class="invalid-feedback d-none" id="error-email" role="alert"></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label class="form-label required" for="input-username">Usuario</label>
                                    <input type="text" class="form-control" id="input-username" name="username" />
                                    <span class="invalid-feedback d-none" id="error-username" role="alert"></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label class="form-label required" for="input-password">Contraseña</label>
                                    <input type="password" class="form-control" id="input-password" name="password" />
                                    <span class="invalid-feedback d-none" id="error-password" role="alert"></span>
                                </div>
                                <div class="col-12">
                                    <label class="form-label required" for="password_confirmation">Confirmar contraseña</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" />
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn me-auto" data-bs-dismiss="modal">Cerrar</a>
                        <button type="submit" form="form-store-user" class="btn btn-primary" onclick="!this.form && document.getElementById('form-store-user').submit()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-edit-user" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Actualizar usuario</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                    </div>
                    <div class="modal-body">
                        <form id="form-update-user" method="POST" action="">
                            @csrf
                            @method("PUT")

                            <div class="row">
                                <div class="col-12 mb-3">
                                    <label class="form-label required" for="input-nombre">Nombre</label>
                                    <input type="text" class="form-control" id="input-nombre" name="nombre" />
                                    <span class="invalid-feedback d-none" id="error-nombre" role="alert"></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label class="form-label" for="input-paterno">Apellido paterno</label>
                                    <input type="text" class="form-control" id="input-paterno" name="paterno" />
                                    <span class="invalid-feedback d-none" id="error-paterno" role="alert"></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label class="form-label" for="input-materno">Apellido materno</label>
                                    <input type="text" class="form-control" id="input-materno" name="materno" />
                                    <span class="invalid-feedback d-none" id="error-materno" role="alert"></span>
                                </div>
                                <div class="col-12 mb-3">
                                    <label class="form-label required" for="input-email">Correo electrónico</label>
                                    <input type="text" class="form-control" id="input-email" name="email" />
                                    <span class="invalid-feedback d-none" id="error-email" role="alert"></span>
                                </div>
                                <div class="col-12">
                                    <label class="form-label required" for="input-username">Usuario</label>
                                    <input type="text" class="form-control" id="input-username" name="username" />
                                    <span class="invalid-feedback d-none" id="error-username" role="alert"></span>
                                </div>
                            </div>
                            <input type="hidden" name="id" id="input-id">
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn me-auto" data-bs-dismiss="modal">Cerrar</a>
                        <button type="submit" form="form-update-user" class="btn btn-primary" onclick="!this.form && document.getElementById('form-update-user').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>

    @endpush

@push("scripts")
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        async function postData(url, data) {
            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                    },
                    body: data
                });

                const contentType = response.headers.get('content-type');

                if ( !contentType || !contentType.includes('application/json') ) {
                    if (response.status === 404) {
                        throw new Error('La acción que intentas realizar no está disponible.');
                    }

                    throw new Error("Por favor, inténtalo de nuevo más tarde.");
                }

                if (!response.ok) {
                    if ( response.status === 401)
                    {
                        throw new Error('Unauthorized');
                    }
                    else if (response.status === 403)
                    {
                        throw new Error('Forbidden');
                    }
                    else if (response.status === 404)
                    {
                        throw new Error('No se encontró el recurso que estás buscando.');
                    }
                    else if (response.status === 422)
                    {
                        console.error('Valida los campos ingresados.');
                    }
                    else
                    {
                        throw new Error('Hubo un problema al procesar la solicitud.');
                    }
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        async function getData(url) {
            try {
                const response = await fetch(url);

                const contentType = response.headers.get('content-type');

                if (!contentType || !contentType.includes('application/json')) {
                    if(response.status === 404) {
                        throw new Error('La acción que intentas realizar no está disponible.');
                    }

                    throw new Error("Por favor, inténtalo de nuevo más tarde.");
                }

                if (!response.ok) {
                    throw new Error("Hubo un problema al obtener el recurso.");
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        function confirmDelete() {
            const button = event.currentTarget;

            const id = button.getAttribute("data-id");

            if ( id && !isNaN(id) && Number.isInteger(parseFloat(id)) ) {
                Swal.fire({
                    title: '¿Estás seguro?',
                    text: "No podrás revertir esta acción",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Eliminar',
                    cancelButtonText: 'Cancelar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        deleteRecord(id);
                    }
                });
            }
        }

        function deleteRecord(id) {
            fetch(`/users/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                }
            })
            .then(response => {

                const contentType = response.headers.get('content-type');

                if ( !contentType || !contentType.includes('application/json') ) {
                    if (response.status === 404) {
                        throw new Error('La acción que intentas realizar no está disponible.');
                    }

                    throw new Error("Por favor, inténtalo de nuevo más tarde.");
                }

                if (!response.ok) {
                    throw new Error('Hubo un problema al eliminar el registro.');
                }

                return response.json();
            })
            .then(result => {
                successAlert(result.message);
            })
            .catch(error => errorAlert(error.message) );
        }

        const tblUsuarios = document.getElementById("tblUsuarios").tBodies[0];

        // Botones de acciones
        const btnEdit = document.querySelectorAll('.btnEdit');
        const btnDelete = document.querySelectorAll('.btnDelete');

        tblUsuarios.addEventListener("click", function(event) {
            const row = event.target.closest("tr");

            // Verificar si se hizo clic en una fila y no en otro elemento dentro de la tabla
            if (row && tblUsuarios.contains(row)) {

                // Si la fila ya está seleccionada, desmarcarla y deshabilitar el botón
                if (row.classList.contains("table-active")) {

                    row.classList.remove("table-active");

                    // Para deshabilitar los botones
                    disableButtons(btnEdit);
                    disableButtons(btnDelete);
                } else {
                    // Eliminar la clase "table-active" de todas las filas
                    const rows = tblUsuarios.getElementsByTagName("tr");

                    for (let i = 0; i < rows.length; i++) {
                        rows[i].classList.remove("table-active");
                    }

                    // Agregar la clase "table-active" a la fila que selecciona
                    row.classList.add("table-active");

                    // Para habilitar los botones y asignarles un id al seleccionar una fila
                    enableButtons(btnEdit, row.id);
                    enableButtons(btnDelete, row.id);
                }

            } else {
                // SI se hizo clic fuera de una fila, ENTONCES deshabilitar los botones
                disableButtons(btnEdit);
                disableButtons(btnDelete);
            }
        });

        function disableButtons(buttons) {
            buttons.forEach(button => {
                button.classList.add("disabled");
                button.removeAttribute("data-id");
            });
        }

        function enableButtons(buttons, id) {
            buttons.forEach(button => {
                button.classList.remove("disabled");
                button.setAttribute("data-id", id);
            });
        }

        disableButtons(btnEdit);
        disableButtons(btnDelete);

        const modalAddUser =  document.getElementById("modal-add-user");

        const formStoreUser = document.getElementById("form-store-user");

        formStoreUser.addEventListener("submit", function(event) {
            event.preventDefault();

            const formData = new FormData(this);
            postData('/users', formData)
            .then(result => {
                if(result.errors)
                {
                    clearInputErrors(formStoreUser);
                    fillInputErrors(formStoreUser, result.errors);
                }
                else
                {
                    successAlert(result.message);
                    const myModal = bootstrap.Modal.getOrCreateInstance(modalAddUser);
                    myModal.hide();
                }
            })
            .catch(error => errorAlert(error.message));
        });

        modalAddUser.addEventListener("hidden.bs.modal", function (event) {
            clearInputErrors(formStoreUser);
            clearFields(formStoreUser);
        });

        const modalEditUser = document.getElementById("modal-edit-user");

        const formUpdateUser = document.getElementById("form-update-user");

        modalEditUser.addEventListener("show.bs.modal", function (event) {
            const modal = this;

            const button = event.relatedTarget;

            const id = button.getAttribute("data-id");

            if( id && !isNaN(id) && Number.isInteger(parseFloat(id)) ) {
                const url = `/users/${id}`;

                getData(url)
                .then(result => {
                    const id = modal.querySelector("#input-id");
                    const inputNombre  = modal.querySelector("#input-nombre");
                    const inputPaterno = modal.querySelector("#input-paterno");
                    const inputMaterno = modal.querySelector("#input-materno");
                    const inputEmail = modal.querySelector("#input-email");
                    const inputUsername = modal.querySelector("#input-username");

                    id.value = result.id;
                    inputNombre.value  = result.nombre;
                    inputPaterno.value = result.paterno;
                    inputMaterno.value = result.materno;
                    inputEmail.value = result.email;
                    inputUsername.value = result.username;
                })
                .catch(error => errorAlert(error.message));
            }
        });

        formUpdateUser.addEventListener("submit", function(event) {
            event.preventDefault();

            const formData = new FormData(this);

            const id = formData.get("id");

            if ( id && !isNaN(id) && Number.isInteger(parseFloat(id)) ) {

                const url = `/users/${id}`;

                postData(url, formData)
                .then(result => {
                    if(result.errors) {
                        clearInputErrors(formUpdateUser);
                        fillInputErrors(formUpdateUser, result.errors);
                    }
                    else {
                        successAlert(result.message);
                        const myModal = bootstrap.Modal.getOrCreateInstance(modalEditUser);
                        myModal.hide();
                    }
                })
                .catch(error => errorAlert(error.message));
            }
        });

        modalEditUser.addEventListener("hidden.bs.modal", function (event) {
            clearInputErrors(formUpdateUser);
            clearFields(formUpdateUser);
        });

        function fillInputErrors(form, errors) {
            Object.keys(errors).forEach(campo => {
                const inputElement = form.querySelector(`[name="${campo}"]`);
                // Verificar si hay errores para este campo en el JSON
                if (errors[campo] && inputElement) {

                    inputElement.classList.add("is-invalid");

                    const spanFeedback = form.querySelector(`#error-${campo}`);

                    const error = errors[campo][0];

                    spanFeedback.classList.remove("d-none");
                    spanFeedback.textContent = error;
                }
            });
        }

        function clearFields(form) {
            inputs = form.getElementsByClassName("form-control");

            for (const key in inputs) {
                inputs[key].value = "";
            }
        }

        function clearInputErrors(form) {
            inputs = form.querySelectorAll("input:not([type='hidden'])");

            for (const input of inputs) {
                const span = input.nextElementSibling;
                if(span){
                    input.classList.remove("is-invalid");
                    span.classList.add("d-none");
                    span.textContent = "";
                }
            }
        }

        function successAlert(message) {
            Swal.fire({
                title: "¡Perfecto!",
                text: message,
                icon: "success",
                showConfirmButton: false,
                timer: 3000
            }).then(() => {
                location.reload();
            });
        }

        function errorAlert(message) {
            Swal.fire('¡Algo salió mal!', message, 'error');
        }
    </script>
@endpush
</x-app-layout>
