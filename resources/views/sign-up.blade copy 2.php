<!doctype html>
<html lang="en" class="h-100" data-bs-theme="auto">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.122.0">
    <title>Cover Template · Bootstrap v5.3</title>


    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        .btn-bd-primary {
            --bd-violet-bg: #712cf9;
            --bd-violet-rgb: 112.520718, 44.062154, 249.437846;

            --bs-btn-font-weight: 600;
            --bs-btn-color: var(--bs-white);
            --bs-btn-bg: var(--bd-violet-bg);
            --bs-btn-border-color: var(--bd-violet-bg);
            --bs-btn-hover-color: var(--bs-white);
            --bs-btn-hover-bg: #6528e0;
            --bs-btn-hover-border-color: #6528e0;
            --bs-btn-focus-shadow-rgb: var(--bd-violet-rgb);
            --bs-btn-active-color: var(--bs-btn-hover-color);
            --bs-btn-active-bg: #5a23c8;
            --bs-btn-active-border-color: #5a23c8;
        }


        .bg-187c {
            color: #fff !important;
            background-color: #ab0033;
        }

        .bg-465c {
            color: #fff !important;
            background-color: #bc955c;
        }

        .bg-468c {
            color: #fff !important;
            background-color: #ddc9a3;
        }

        .bg-cool-gray {
            color: #fff !important;
            background-color: #54565a;
        }

        .card-stamp {
            --tblr-stamp-size: 7rem;
            position: absolute;
            top: 0;
            right: 0;
            width: calc(var(--tblr-stamp-size) * 1);
            height: calc(var(--tblr-stamp-size) * 1);
            max-height: 100%;
            border-top-right-radius: 4px;
            opacity: 0.2;
            overflow: hidden;
            pointer-events: none;
        }

        .card-stamp-lg {
            --tblr-stamp-size: 13rem;
        }

        .card-stamp-icon {
            background: var(--tblr-secondary);
            color: var(--tblr-card-bg, var(--tblr-bg-surface));
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 100rem;
            width: calc(var(--tblr-stamp-size) * 1);
            height: calc(var(--tblr-stamp-size) * 1);
            position: relative;
            top: calc(var(--tblr-stamp-size) * -0.25);
            right: calc(var(--tblr-stamp-size) * -0.25);
            font-size: calc(var(--tblr-stamp-size) * 0.75);
            transform: rotate(10deg);
        }

        .card-stamp-icon .icon {
            stroke-width: 2;
            width: calc(var(--tblr-stamp-size) * 0.75);
            height: calc(var(--tblr-stamp-size) * 0.75);
        }
    </style>

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/cover.css') }}" rel="stylesheet">
</head>

<body class="d-flex h-100 text-center bg-468c">


    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
        <header class="mb-auto">
            <div>
                <h3 class="float-md-start mb-0">Cover</h3>
                <nav class="nav nav-masthead justify-content-center float-md-end">
                    <a class="nav-link fw-bold py-1 px-0 active" aria-current="page" href="#">Home</a>
                    <a class="nav-link fw-bold py-1 px-0" href="#">Features</a>
                    <a class="nav-link fw-bold py-1 px-0" href="#">Contact</a>
                </nav>
            </div>
        </header>

        {{-- <main class=""> --}}



        {{-- <div class="container"> --}}
        <div class="card">
            <div class="card-stamp card-stamp-lg">
                <div class="card-stamp-icon bg-white">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                        fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                        stroke-linejoin="round" class="icon">
                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                        <path
                            d="M12 17.75l-6.172 3.245l1.179 -6.873l-5 -4.867l6.9 -1l3.086 -6.253l3.086 6.253l6.9 1l-5 4.867l1.179 6.873z">
                        </path>
                    </svg>
                </div>
            </div>
            <div class="card-body bg-187c">
                <h2 class="h2 text-center mb-4">Ingrese a su cuenta</h2>

                <form action="{{-- route('login') --}}" method="post">
                    @csrf

                    <div class="mb-3">
                        <label class="form-label" for="email">Correo electrónico</label>
                        <input type="text" id="email" name="email"
                            class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                            autofocus required>
                        @error('email')
                            <span class="invalid-feedback" role="alert">{{ $message }}</span>
                        @enderror

                    </div>

                    <div class="mb-2">
                        <label class="form-label" for="password">
                            Contraseña
                            <span class="form-label-description">
                                <a href="#" tabindex="-1">Olvidé mi contraseña</a>
                            </span>
                        </label>

                        <div class="row g-2">
                            <div class="col">
                                <input type="password" id="password" name="password"
                                    class="form-control @error('password') is-invalid @enderror" autocomplete="off"
                                    required>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">{{ $message }}</span>
                                @enderror
                            </div>

                            <div class="col-auto">
                                <button type="button" class="btn btn-icon" onclick="showPassword()" title="Mostrar"
                                    data-bs-toggle="tooltip" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                        stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                        <circle cx="12" cy="12" r="2" />
                                        <path
                                            d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <label class="form-check">
                            <input type="checkbox" class="form-check-input" />
                            <span class="form-check-label">Recordarme</span>
                        </label>
                    </div>
                    <div class="form-footer">
                        <button type="submit" class="btn btn-primary w-100">Ingresar</button>
                    </div>
                </form>
            </div> <!-- /card-body -->

            <svg xmlns="http://www.w3.org/2000/svg" viewBox="150 20 1140 200" style="background: #DBCBB0;">
                <path fill="#BD9D84" fill-opacity="1"
                    d="M0,256L48,218.7C96,181,192,107,288,69.3C384,32,480,32,576,58.7C672,85,768,139,864,170.7C960,203,1056,213,1152,229.3C1248,245,1344,267,1392,277.3L1440,288L1440,320L1392,320C1344,320,1248,320,1152,320C1056,320,960,320,864,320C768,320,672,320,576,320C480,320,384,320,288,320C192,320,96,320,48,320L0,320Z">
                </path>
            </svg>

        </div> <!-- /card -->

        {{-- </div> --}}

        {{-- </main> --}}

        <footer class="mt-auto text-white-50">
            <p>Cover template for <a href="https://getbootstrap.com/" class="text-white">Bootstrap</a>, by <a
                    href="https://twitter.com/mdo" class="text-white">@mdo</a>.</p>
        </footer>
    </div>
    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>

</body>

</html>
