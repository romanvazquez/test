<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? config('app.name') }}</title>

    <link rel="stylesheet" href="{{ asset('css/tabler-new.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/tabler-vendors.css') }}">
    @stack('styles')

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="page">
        <!-- Navbar -->
        <header class="navbar navbar-expand-md d-print-none">
            <div class="container-xl">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu"
                    aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <!-- Header logo -->
                <div class="navbar-brand d-none-navbar-horizontal pe-0 pe-md-3">
                    <!-- <img src="{{asset('img/')}}" width="110" height="32" alt="" class="navbar-brand-image"> -->
                </div>
                <div class="navbar-nav flex-row order-md-last">

                    @auth

                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown"
                            aria-label="Abrir menú de usuario">
                            <span class="avatar avatar-sm">{{ auth()->user()->iniciales }}</span>
                            <div class="d-none d-xl-block ps-2">
                                <div>{{ auth()->user()->nombre_completo }}</div>
                                <div class="mt-1 small text-muted">{{ auth()->user()->email }}</div>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                            <a href="#" class="dropdown-item" onclick="document.getElementById('logout-form').submit()">
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon dropdown-item-icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                    <path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2"></path>
                                    <path d="M7 12h14l-3 -3m0 6l3 -3"></path>
                                </svg>
                                Cerrar sesión
                            </a>
                            <form action="{{ route('logout') }}" method="POST" id="logout-form">
                                @csrf
                            </form>
                        </div>
                    </div>
                    @endauth

                </div>
            </div>
        </header>
        <div class="navbar-expand-md">
            <div class="collapse navbar-collapse" id="navbar-menu">
                <div class="navbar navbar-light">
                    <div class="container-xl">

                        <x-menu-layout/>

                    </div>
                </div>
            </div>
        </div>
        <div class="page-wrapper">

            {{ $slot }}

            <x-footer-layout/>

        </div>
    </div>

    @stack('modals')

    <script src="{{asset('js/tabler.min.js')}}" defer></script>

    @stack('scripts')

</body>
</html>
