<ul class="navbar-nav pt-lg-3">
    <li class="nav-item">
        <a class="nav-link" href="{{route('home')}}">
            <span class="nav-link-icon">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path d="M5 12l-2 0l9 -9l9 9l-2 0" />
                    <path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7" />
                    <path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6" />
                </svg>
            </span>
            <span class="nav-link-title">
                Inicio
            </span>
        </a>
    </li>
    <li class="nav-item active dropdown">
        <a class="nav-link dropdown-toggle" href="#navbar-base" data-bs-toggle="dropdown"
            data-bs-auto-close="false" role="button" aria-expanded="false">
            <span class="nav-link-icon">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path d="M12 3l8 4.5l0 9l-8 4.5l-8 -4.5l0 -9l8 -4.5" />
                    <path d="M12 12l8 -4.5" />
                    <path d="M12 12l0 9" />
                    <path d="M12 12l-8 -4.5" />
                    <path d="M16 5.25l-8 4.5" />
                </svg>
            </span>
            <span>
                Dropdown
            </span>
        </a>
        <div class="dropdown-menu">
            <div class="dropdown-menu-columns">
                <div class="dropdown-menu-column">
                    <a class="dropdown-item" href="#">
                        Item
                    </a>
                    <div class="dropend">
                        <a class="dropdown-item dropdown-toggle" href="#sidebar-authentication"
                            data-bs-toggle="dropdown" data-bs-auto-close="false" role="button"
                            aria-expanded="false">
                            Multilevel
                        </a>
                        <div class="dropdown-menu">
                            <a href="#" class="dropdown-item">
                                Level 1
                            </a>
                            <a href="#" class="dropdown-item">
                                Level 2
                            </a>
                            <a href="#" class="dropdown-item">
                                Level 3
                            </a>
                        </div>
                    </div>
                </div>
                <div class="dropdown-menu-column"></div>
            </div>
        </div>
    </li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#navbar-extra" data-bs-toggle="dropdown"
            data-bs-auto-close="false" role="button" aria-expanded="false">
            <span class="nav-link-icon">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                    stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                    <path
                        d="M12 17.75l-6.172 3.245l1.179 -6.873l-5 -4.867l6.9 -1l3.086 -6.253l3.086 6.253l6.9 1l-5 4.867l1.179 6.873z" />
                </svg>
            </span>
            <span class="nav-link-title">
                Usuarios
            </span>
        </a>
        <div class="dropdown-menu">
            <div class="dropdown-menu-columns">
                <div class="dropdown-menu-column">
                    <a class="dropdown-item" href="#">
                        Usuarios
                    </a>
                    <a class="dropdown-item" href="#">
                        Roles
                    </a>
                    <a class="dropdown-item" href="#">
                        Permisos
                    </a>
                </div>
                <div class="dropdown-menu-column"></div>
            </div>
        </div>
    </li>
</ul>
