<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title ?? config('app.name') }}</title>
    <link rel="stylesheet" href="{{ asset('css/tabler.css') }}">
    @stack('styles')

    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <!-- <script src="{{-- asset('js/demo-theme.min.js') --}}"></script> -->
    <script src="{{ asset('js/tabler.min.js') }}"></script>
    <div class="page">
        <!-- Sidebar -->
        <aside class="navbar navbar-vertical navbar-expand-lg" data-bs-theme="dark">
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#sidebar-menu"
                    aria-controls="sidebar-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <h1 class="navbar-brand">
                    <a href="#">
                        <img src="" width="110" height="32" alt="LOGO" class="navbar-brand-image">
                    </a>
                </h1>
                <div class="navbar-nav flex-row d-lg-none">
                    <div class="nav-item dropdown">
                        <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown"
                            aria-label="Open user menu">
                            <span class="avatar avatar-sm" style="background-image: url()">RV</span>
                            <div class="d-none d-xl-block ps-2">
                                <div>Nombre</div>
                                <div class="mt-1 small text-secondary">Rol</div>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                            <a href="#" class="dropdown-item">Rol 1</a>
                            <a href="#" class="dropdown-item">Rol 2</a>
                            <div class="dropdown-divider"></div>
                            <a href="./sign-in.html" class="dropdown-item">Cerrar sesión</a>
                        </div>
                    </div>
                </div>

                <!-- Menú de la Sidebar -->
                <div class="collapse navbar-collapse" id="sidebar-menu">

                    <x-vertical-menu-layout />

                </div>
            </div>
        </aside>
        <header
            class="navbar navbar-expand-md d-flex flex-wrap align-items-center justify-content-center justify-content-md-between p-3 border-bottom">
            <div class="nav col-12 justify-content-center">
                <div class="navbar-brand">
                    <!-- <img src="{{asset('img/membrete.svg')}}" width="110" height="32" alt="SET" class="navbar-brand-image"> -->
                </div>
            </div>
        </header>
        <div class="page-wrapper">
            {{ $slot }}
            <!-- Aquí va el footer -->
        </div>
    </div>

    @stack('scripts')
</body>

</html>
