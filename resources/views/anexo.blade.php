<!DOCTYPE html>
<html>
<head>
    <title>Listado de Anexos</title>
    <meta charset="UTF-8">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        /* table, th, td {
            border: 1px solid black;
        } */

        table {
            width: 100%;
            border-collapse: collapse;
        }
        td, th {
            border: 1px solid #000;
            padding: 10px;
        }
        .textarea-full-width {
            width: 100%;
            box-sizing: border-box; /* Esto asegura que el padding y border se incluyan en el 100% del ancho */
        }
    </style>
</head>
<body>
    <table width="100%">
        <thead>
            <tr>
                <th colspan="3">
                    {{ $consecutivo. " - ". $concepto }}
                </th>
            </tr>
        </thead>
        <tbody>
            {{-- <tr>
                <td width="20%">Modelos</td>
                <td width="40%"><form action=""><textarea name="modelos" class="textarea-full-width">{{$modelos}}</textarea><br><button type="submit" class="textarea-full-width">UPDT</button></form></td>
                <td width="40%">{{$modelos}}</td>
            </tr> --}}
            <tr>
                <td width="20%">Modelos</td>
                <td colspan="2" style="text-align: center;">{{$modelos}}</td>
            </tr>
            <tr>
                <td width="20%">Requisitos</td>
                <td width="40%"><form action="{{ route('requisitos.update', ['requisitos' => $consecutivo]) }}" method="POST">@csrf @method('PUT') <textarea name="requisitos" class="textarea-full-width" cols="30" rows="10">{{$requisitos}}</textarea><br><button type="submit" class="textarea-full-width">UPDT</button></form></td>
                <td width="40%">{!! $requisitos !!}</td>
            </tr>
            <tr>
                <td width="20%">Fundamento</td>
                <td width="40%"><form action="{{ route('fundamento.update', ['fundamento' => $consecutivo]) }}" method="POST">@csrf @method('PUT') <textarea name="fundamento" class="textarea-full-width" cols="30" rows="10">{{$fundamento}}</textarea><br><button type="submit" class="textarea-full-width">UPDT</button></form></td>
                <td width="40%">{!! $fundamento !!}</td>
            </tr>
            <tr>
                <td width="20%">Excepciones</td>
                <td width="40%"><form action="{{ route('excepciones.update', ['excepciones' => $consecutivo]) }}" method="POST">@csrf @method('PUT') <textarea name="excepciones" class="textarea-full-width" cols="30" rows="5">{{$excepciones}}</textarea><br><button type="submit" class="textarea-full-width">UPDT</button></form></td>
                <td width="40%">{!! $excepciones !!}</td>
            </tr>
            <tr>
                <td width="20%">Periodicidad</td>
                <td width="40%"><form action="{{ route('periodicidad.update', ['periodicidad' => $consecutivo]) }}" method="POST">@csrf @method('PUT') <textarea name="periodicidad" class="textarea-full-width" cols="30" rows="5">{{$periodicidad}}</textarea><br><button type="submit" class="textarea-full-width">UPDT</button></form></td>
                <td width="40%">{!! $periodicidad !!}</td>
            </tr>
            <tr>
                <td width="20%">Observaciones</td>
                <td width="40%"><form action="{{ route('observaciones.update', ['observaciones' => $consecutivo]) }}" method="POST">@csrf @method('PUT') <textarea name="observaciones" class="textarea-full-width" cols="30" rows="10">{{$observaciones}}</textarea><br><button type="submit" class="textarea-full-width">UPDT</button></form></td>
                <td width="40%">{!! $observaciones !!}</td>
            </tr>
            <tr>
                <td width="20%">Consideraciones</td>
                <td width="40%"><form action="{{ route('consideraciones.update', ['consideraciones' => $consecutivo]) }}" method="POST">@csrf @method('PUT') <textarea name="consideraciones" class="textarea-full-width" cols="30" rows="10">{{$consideraciones}}</textarea><br><button type="submit" class="textarea-full-width">UPDT</button></form></td>
                <td width="40%">{!! $consideraciones !!}</td>
            </tr>
        </tbody>
    </table>

    <script>

    const forms = document.querySelectorAll('form');

    forms.forEach((form) => {
        const textareas = form.querySelectorAll('textarea');

        textareas.forEach((textarea) => {
            textarea.addEventListener('keydown', function(event) {
                if (event.key === 'Enter' && !event.shiftKey) {
                    event.preventDefault();
                    form.submit();
                }
            });
        });
    });
    </script>
</body>
</html>
