<x-app-layout>

    @push('styles')
        <style>
            .verti-sitemap a {
            color: var(--tblr-body-color);
            display: block;
            }
            .verti-sitemap .parent-title a {
            padding-right: 0;
            }
            .verti-sitemap .parent-title a:before {
            display: none;
            }
            .verti-sitemap .parent-title:before {
            display: none;
            }
            .verti-sitemap .first-list {
            position: relative;
            padding-top: 10px;

            /*  */
            margin-left: 24px;
            }
            .verti-sitemap .first-list:before {
            content: "";
            border-left: 2px dashed var(--tblr-border-color);
            position: absolute;
            top: 0;
            height: 100%;
            bottom: 0;
            /* right: 0; */

            /*  */
            left: -24px;
            }
            .verti-sitemap .first-list .list-wrap a, .verti-sitemap .first-list li a {
            position: relative;
            padding: 10px 36px 4px 16px;
            }
            .verti-sitemap .first-list .list-wrap a::before, .verti-sitemap .first-list li a::before {
            content: "";
            width: 24px;
            border-top: 2px dashed var(--tblr-border-color);
            position: absolute;
            top: 50%;
            -webkit-transform: translateY(-50%);
                    transform: translateY(-50%);
            /* right: 0; */
            left: -24px;
            }
            .verti-sitemap .first-list .second-list, .verti-sitemap .first-list .third-list {
            margin-left: 42px;
            }
            .verti-sitemap .first-list .third-list, .verti-sitemap .first-list .second-list {
            position: relative;
            }
            .verti-sitemap .first-list .third-list li, .verti-sitemap .first-list .second-list li {
            position: relative;
            }
            .verti-sitemap .first-list .third-list li:before, .verti-sitemap .first-list .second-list li:before {
            content: "";
            height: 100%;
            border-left: 2px dashed var(--tblr-border-color);
            position: absolute;
            top: 0;
            /* right: 0; */
            /* margin: 0px auto; */
            left: -24px;
            }
            .verti-sitemap .first-list .third-list li:last-child::before, .verti-sitemap .first-list .second-list li:last-child::before {
            height: 13px;
            }
            .verti-sitemap .first-list:last-child::before {
            height: 25px;
            }
        </style>
    @endpush
    <div class="page-header">
        <div class="container-xl">
            <div class="row align-items-center">
                <div class="col">
                    <h2 class="page-title">Categorías</h2>
                </div>
                <div class="col-auto ms-auto d-print-none">

                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="container-xl">
            <div class="row row-cards">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Categorías</h3>
                            <div class="card-actions">
                                <a href="#" class="btn btn-primary" role="button" data-bs-toggle="modal"
                                    data-bs-target="#modal-add-category">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                        viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                        stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                        <line x1="12" y1="5" x2="12" y2="19" />
                                        <line x1="5" y1="12" x2="19" y2="12" />
                                    </svg>
                                    Agregar
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="verti-sitemap">
                                <ul class="list-unstyled mb-0">
                                    <li class="p-0 parent-title">
                                        <a href="#" class="fw-medium">Categorías</a>
                                    </li>
                                    <li>
                                        @foreach ($post_categories as $category)
                                            <div class="first-list">
                                                <div class="list-wrap">
                                                    <a href="#" class="fw-semibold sub-title" data-bs-toggle="modal" data-bs-target="#modal-edit-category" data-categoryid="{{$category->id}}">{{ $category->title }}</a>
                                                </div>
                                                @if ( isset($category->children) )
                                                    <ul class="second-list list-unstyled">
                                                        @foreach ($category->children as $child)
                                                            @include('post-categories.partials.child', ['child' => $child])
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </div>
                                        @endforeach
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">

                </div>
            </div>
        </div>
    </div>

    @push('modals')
        <!-- Categorías -->
        <div class="modal fade" id="modal-add-category" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Agregar categoría</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('post-categories.store') }}" method="POST" id="form-store-category">
                            @csrf

                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-3">
                                        <label class="form-label required" for="input-title">Nombre</label>
                                        <input type="text" class="form-control" id="input-title" name="title" />
                                        <span class="invalid-feedback d-none" id="error-title" role="alert"></span>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <select class="form-select" name="parent_id" aria-label="" >
                                        <option selected></option>
                                        @foreach ($categories_options as $item)

                                        <option value="{{$item->id}}">{!! $item->title !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn me-auto" data-bs-dismiss="modal">Cerrar</a>
                        <button type="submit" form="form-store-category" class="btn btn-primary"
                            onclick="!this.form && document.getElementById('form-store-category').submit()">Agregar</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-edit-category" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Actualizar categoría</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Cerrar"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('post-categories.update') }}" method="POST" id="form-update-category">
                            @csrf
                            @method('PUT')

                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-3">
                                        <label class="form-label required" for="input-title">Título</label>
                                        <input type="text" class="form-control" id="input-title" name="title" />
                                        <span class="invalid-feedback d-none" id="error-title" role="alert"></span>
                                    </div>
                                    <input type="hidden" name="categoryId" id="input-category-id">
                                </div>
                                <div class="col-12">
                                    <select class="form-select" name="parent_id" aria-label="" id="slct-categories">
                                        <option selected></option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn me-auto" data-bs-dismiss="modal">Cerrar</a>
                        <button type="submit" form="form-update-category" class="btn btn-primary"
                            onclick="!this.form && document.getElementById('form-update-category').submit()">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
    @endpush

    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

        <script>
            async function postData(url, data) {
                try {
                    const response = await fetch(url, {
                        method: 'POST',
                        headers: {
                            "X-Requested-With": "XMLHttpRequest",
                        },
                        body: data
                    });

                    // const contentType = response.headers.get('content-type');

                    // if (!contentType || !contentType.includes('application/json')) {
                    //     if (response.status === 404) {
                    //         throw new Error('La acción que intentas realizar no está disponible.');
                    //     }

                    //     throw new Error("Por favor, inténtalo de nuevo más tarde.");
                    // }

                    if (!response.ok) {
                        if (response.status === 401) {
                            throw new Error('Unauthorized');
                        } else if (response.status === 403) {
                            throw new Error('Forbidden');
                        } else if (response.status === 404) {
                            throw new Error('No se encuentra el recurso que estás buscando.');
                        } else if (response.status === 422) {
                            console.error('Valida los campos ingresados.');
                        } else {
                            throw new Error('Hubo un problema al procesar la solicitud.');
                        }
                    }

                    const result = await response.json();
                    return result;
                } catch (error) {
                    throw error;
                }
            }

            async function getData(url) {
                try {
                    const response = await fetch(url);

                    // const contentType = response.headers.get('content-type');

                    // if (!contentType || !contentType.includes('application/json')) {
                    //     if (response.status === 404) {
                    //         throw new Error('La acción que intentas realizar no está disponible');
                    //     }

                    //     throw new Error("Por favor, inténtalo de nuevo más tarde.");
                    // }

                    if (!response.ok) {
                        if (response.status === 401) {
                            throw new Error('Unauthorized');
                        } else if (response.status === 403) {
                            throw new Error('Forbidden');
                        } else if (response.status === 404) {
                            throw new Error('No se encuentra el recurso que estás buscando.');
                        } else if (response.status === 422) {
                            console.error('Valida los campos ingresados.');
                        } else {
                            throw new Error('Hubo un problema al obtener el recurso.');
                        }
                    }

                    // if (!response.ok) {
                    //     throw new Error("Hubo un problema al obtener el recurso.");
                    // }

                    const result = await response.json();
                    return result;
                } catch (error) {
                    throw error;
                }
            }

            // Categorías
            const modalAddCategory = document.getElementById("modal-add-category");

            const formStoreCategory = document.getElementById("form-store-category");

            formStoreCategory.addEventListener("submit", function(event) {
                event.preventDefault();

                const url = formStoreCategory.action;

                const formData = new FormData(formStoreCategory);

                postData(url, formData)
                    .then(result => {
                        if (result.errors) {
                            clearInputErrors(formStoreCategory);
                            fillInputErrors(formStoreCategory, result.errors);
                        } else {
                            successAlertWithReload(result.message);
                            const myModal = bootstrap.Modal.getOrCreateInstance(modalAddCategory);
                            myModal.hide();
                        }
                    })
                    .catch(error => errorAlert(error.message));
            });

            modalAddCategory.addEventListener("hidden.bs.modal", function(event) {
                clearInputErrors(formStoreCategory);
                clearFields(formStoreCategory);
            });

            const modalEditCategory = document.getElementById("modal-edit-category");

            const formUpdateCategory = document.getElementById("form-update-category");

            modalEditCategory.addEventListener("show.bs.modal", function (event) {
                const modal = this;

                const button = event.relatedTarget;

                const id = button.getAttribute("data-categoryid");

                if( id && !isNaN(id) && Number.isInteger(parseFloat(id)) ) {
                    const slctCategories = modal.querySelector("#slct-categories");
                    const inputCategoryId = modal.querySelector("#input-category-id");
                    const inputTitle = modal.querySelector("#input-title");

                    getData( `/post-categories/show-options/${id}` )
                    .then(categories => {
                        populateCategories(categories, slctCategories);

                        return getData( `/post-categories/${id}` );
                    })
                    .then(category => {
                        inputCategoryId.value = category.id;
                        inputTitle.value  = category.title;
                        slctCategories.value = category.parent_id;
                    })
                    .catch( error => errorAlert(error.message) );
                }
            });

            formUpdateCategory.addEventListener("submit", function(event) {
                event.preventDefault();

                const formData = new FormData(formUpdateCategory);

                const url = formUpdateCategory.action;

                const categoryId = formData.get("categoryId");

                if( categoryId && !isNaN(categoryId) && Number.isInteger(parseFloat(categoryId)) ) {

                    postData(`${url}/${categoryId}`, formData)
                    .then(result => {
                        if(result.errors) {
                            clearInputErrors(formUpdateCategory);
                            fillInputErrors(formUpdateCategory, result.errors);
                        }
                        else {
                            successAlertWithReload(result.message);
                            const myModal = bootstrap.Modal.getOrCreateInstance(modalEditCategory);
                            myModal.hide();
                        }
                    })
                    .catch(error => errorAlert(error.message));
                }
            });

            modalEditCategory.addEventListener("hidden.bs.modal", function (event) {
                clearInputErrors(formUpdateCategory);
                clearFields(formUpdateCategory);
            });

            function populateCategories(categories, slctCategories) {
                // Limpiar las opciones
                slctCategories.innerHTML = "";

                const options = categories.map(category => {
                    return `<option value="${category.id}">${category.title}</option>`;
                });

                // Agregar como primera opción el por defecto
                options.unshift('<option value=""></option>');

                const string = options.join('');

                // Insertar las opciones en el elemento Select
                slctCategories.innerHTML = string;
            }

            function fillInputErrors(form, errors) {
                Object.keys(errors).forEach(campo => {
                    const inputElement = form.querySelector(`[name="${campo}"]`);
                    // Verificar si hay errores para este campo en el JSON
                    if (errors[campo] && inputElement) {

                        inputElement.classList.add("is-invalid");

                        const spanFeedback = form.querySelector(`#error-${campo}`);

                        const error = errors[campo][0];

                        spanFeedback.classList.remove("d-none");
                        spanFeedback.textContent = error;
                    }
                });
            }

            function clearFields(form) {
                const inputs = form.querySelectorAll(".form-control, .form-select");

                for (const key in inputs) {
                    inputs[key].value = "";
                }
            }

            function clearInputErrors(form) {
                inputs = form.querySelectorAll("input:not([type='hidden'])");
                // inputs = form.querySelectorAll("input:not([type='hidden']):not([type='checkbox'])");

                for (const input of inputs) {
                    const span = input.nextElementSibling;
                    if (span) {
                        input.classList.remove("is-invalid");
                        span.classList.add("d-none");
                        span.textContent = "";
                    }
                }
            }

            function successAlertWithReload(message) {
                Swal.fire({
                    title: "¡Perfecto!",
                    text: message,
                    icon: "success",
                    showConfirmButton: false,
                    timer: 2500
                }).then(() => {
                    location.reload();
                });
            }

            function errorAlert(message) {
                Swal.fire('¡Algo salió mal!', message, 'error');
            }
        </script>
    @endpush
</x-app-layout>
