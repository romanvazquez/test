<li>
    <a href="#" data-bs-toggle="modal" data-bs-target="#modal-edit-category" data-categoryid="{{$child->id}}">{{ $child->title }}</a>

    @if ( isset($child->children) )
        <ul class="{{ $loop->depth === 2 ? 'third-list' : 'second-list' }} list-unstyled">

            @foreach ($child->children as $grandChild)
                @include('post-categories.partials.child', ['child' => $grandChild])
            @endforeach
        </ul>
    @endif
</li>
