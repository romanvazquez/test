<!doctype html>
<html lang="en" class="h-100" data-bs-theme="auto">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.122.0">
    <title>Cover Template · Bootstrap v5.3</title>

    {{-- <link rel="canonical" href="https://getbootstrap.com/docs/5.3/examples/cover/"> --}}

    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@docsearch/css@3"> --}}


    <link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .b-example-divider {
            width: 100%;
            height: 3rem;
            background-color: rgba(0, 0, 0, .1);
            border: solid rgba(0, 0, 0, .15);
            border-width: 1px 0;
            box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
        }

        .b-example-vr {
            flex-shrink: 0;
            width: 1.5rem;
            height: 100vh;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: flex;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }

        /* .btn-bd-primary {
            --bd-violet-bg: #712cf9;
            --bd-violet-rgb: 112.520718, 44.062154, 249.437846;

            --bs-btn-font-weight: 600;
            --bs-btn-color: var(--bs-white);
            --bs-btn-bg: var(--bd-violet-bg);
            --bs-btn-border-color: var(--bd-violet-bg);
            --bs-btn-hover-color: var(--bs-white);
            --bs-btn-hover-bg: #6528e0;
            --bs-btn-hover-border-color: #6528e0;
            --bs-btn-focus-shadow-rgb: var(--bd-violet-rgb);
            --bs-btn-active-color: var(--bs-btn-hover-color);
            --bs-btn-active-bg: #5a23c8;
            --bs-btn-active-border-color: #5a23c8;
        } */

        .bg-187c {
            color:#fff!important;
            /* background-color:#ab0033; */

            background-color:RGBA( 171,0 , 51, var(--bs-bg-opacity, 1) )!important;
        }

        /* .bg-187c {
            color:#fff!important;ab0033
            background-color:RGBA( var(--bs-dark-rgb), var(--bs-bg-opacity, 1) )!important;
        } */


        /* A partir de aquí todo es extraído del tema Velzon */
        .bg-cover-187c {
            /* background: linear-gradient(45deg, #ab0033 50%, #570920); */
            background: linear-gradient(45deg, #570920 50%, #ab0033);
        }

        .bg-cover-187c > .bg-overlay {
            /* background-image: url("../images/cover-pattern.png"); */
            background-color: #000;
            background-position: center;
            background-size: cover;
            opacity: 1;
            background-color: transparent;
        }

        .bg-cover-187c .footer {
            color: rgba(255, 255, 255, 0.5);
        }

        .img-responsive.img-responsive-21x9.card-img-top::before {
            content: "";
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: linear-gradient(rgba(171, 0, 51, 0.5), rgba(221, 201, 163, 0.5));
            /* Ajusta los colores y la opacidad según sea necesario */
            z-index: 1;
            pointer-events: none;
        }

        .img-responsive.img-responsive-21x9.card-img-top {
            position: relative;
            background-size: cover;
            background-position: center;
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="{{asset('css/cover.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">




</head>

<body>

    <div class="auth-page-wrapper bg-cover-187c py-5 d-flex justify-content-center align-items-center min-vh-100">
        <div class="bg-overlay"></div>
        <!-- auth-page content -->
        <div class="auth-page-content overflow-hidden pt-lg-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card overflow-hidden border-0 m-0">
                            <div class="row justify-content-center g-0">
                                <div class="col-lg-6">
                                    <div class="p-lg-5 p-4 auth-one-bg h-100">
                                        <div class="bg-overlay"></div>
                                        <div class="position-relative h-100 d-flex flex-column">
                                            <div class="mb-4">
                                                <a href="index.html" class="d-block">
                                                    <img src="assets/images/logo-light.png" alt="" height="18">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="p-lg-5 p-4">
                                        <div>
                                            <h5 class="text-primary">Registro de usuario</h5>
                                        </div>

                                        <div class="mt-4">
                                            <form>

                                                <div class="mb-3">
                                                    <label for="rfc-input" class="form-label">RFC</label>
                                                    <input type="email" class="form-control" id="rfc-input" name="rfc" placeholder="Ingrese su RFC" required>
                                                    <!-- <div class="invalid-feedback">
                                                        Please enter email
                                                    </div> -->
                                                </div>
                                                <div class="mb-3">
                                                    <label for="email-input" class="form-label">Correo electrónico</label>
                                                    <input type="email" class="form-control" id="email-input" name="email" placeholder="Ingrese su dirección de correo electrónico" required>
                                                    <!-- <div class="invalid-feedback">
                                                        Please enter username
                                                    </div> -->
                                                </div>

                                                <div class="mb-3">
                                                    <label class="form-label" for="password-input">Contraseña</label>
                                                    <div class="position-relative auth-pass-inputgroup">
                                                        <input type="password" class="form-control pe-5 password-input" placeholder="Ingrese su contraseña" id="password-input" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
                                                        <button class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted password-addon" type="button" id="password-addon">
                                                            <i class="ri-eye-fill align-middle"></i>
                                                        </button>
                                                        <!-- <div class="invalid-feedback">
                                                            Please enter password
                                                        </div> -->
                                                    </div>
                                                </div>

                                                <div class="mb-3">
                                                    <label class="form-label" for="password-input">Confirmar contraseña</label>
                                                    <input type="password" class="form-control pe-5 password-input" placeholder="Confirme su contraseña" id="password-input" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required>
                                                </div>

                                                <div id="password-contain" class="p-3 bg-light mb-2 rounded">
                                                    <h5 class="fs-13 fw-semibold">La contraseña debe contener:</h5>
                                                    <p id="pass-length" class="invalid fs-12 mb-2">Un mínimo de <b>6 caracteres</b></p>
                                                </div>

                                                <div class="mt-4">
                                                    <button class="btn btn-success w-100" type="submit">Registrarme</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end auth page content -->

        <!-- footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <p class="mb-0">&copy;
                                <script>document.write(new Date().getFullYear())</script> Velzon. Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesbrand
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>

</body>

</html>
