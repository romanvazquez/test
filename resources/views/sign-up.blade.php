<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="light" data-sidebar-size="lg" data-sidebar-image="none" data-preloader="disable">

<head>
    <meta charset="utf-8" />
    <title>Regístrate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Layout config Js -->
    {{-- <script src="{{asset('js/layout.js')}}"></script> --}}

    <!-- Bootstrap Css -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />

    <style>
        /* Estas son sobre-escrituras de bootstrap */
        .nav-pills .nav-link.active,
        .nav-pills .show > .nav-link {
            background-color: #ab0033 !important;
        }

        .alert-c1 {
            --alert-color: #54565a;   /* Color de texto en blanco para buen contraste */
            --alert-bg: #f4d8dd;      /* Fondo con el color atenuado */
            --alert-border-color: #914155; /* Borde ligeramente más oscuro */
            --alert-link-color: #ddc9a3;   /* Enlaces en un tono beige claro */

            color: var(--alert-color);
            background-color: var(--alert-bg);
            border-color: var(--alert-border-color);
        }

        .alert-c1 a {
            color: var(--alert-link-color);
            text-decoration: underline;
        }

        .alert-c1 a:hover {
            color: darken(var(--alert-link-color), 10%);
        }

        .alert-c2 {
            --alert-color: #6d6e71;  /* Color de texto más claro */
            --alert-bg: #f4eee3;     /* Fondo más suave */
            --alert-border-color: #e3c8a3; /* Borde más claro y tenue */
            --alert-link-color: #a74d65;   /* Enlace en un tono rosa apagado */

            color: var(--alert-color);
            background-color: var(--alert-bg);
            border-color: var(--alert-border-color);
        }

        .alert-c2 a {
            color: var(--alert-link-color);
            text-decoration: underline;
        }

        .alert-c2 a:hover {
            color: darken(var(--alert-link-color), 10%);
        }

        /* Esto francamente debió ir en el archivo app */
        .progress-c2 .progress-bar {
            background-color: #bc955c;
        }
        .progress-c2 .progress-bar::after {
            border-right-color: #bc955c;
        }
        .progress-c2 .progress-bar:nth-child(2) {
            background-color: rgba(37, 160, 226, 0.1) !important;
            color: #bc955c !important;
        }
        .progress-c2 .progress-bar:nth-child(2)::after {
            border-right-color: rgba(37, 160, 226, 0.1);
        }

        .alert-additional.alert-c2 .alert-content {
            background-color: #e3c8a3;
            color: #fff;
        }

        .bg-c2 {
            background: #bc955c !important;
        }

        /* Botones personalizados */
        .btn-c1-light {
            color: #ab0033;
            border-color: #be0a40;
            /* border-radius: 15px; */
            background: white;
            cursor: pointer;
        }

        .btn-c1-light:hover {
            color: white !important;
            border-color: #ab0033;
            background: #ab0033;
        }

        .btn-c2-light {
            color: #bc955c;
            border-color: #bc955c;
            border-radius: 15px;
            background: white;
            cursor: pointer;
        }

        .btn-c2-light:hover {
            color: white !important;
            border-color: #bc955c;
            background: #bc955c;
        }

        .btn-c3-light {
            cursor: pointer;
            background: white;
            color: #ddc9a3;
            /* border-radius: 15px; */
            border-color: #ddc9a3;
        }

        .btn-c3-light:hover {
            color: white !important;
            border-color: #ddc9a3;
            background: #ddc9a3;
        }

        .btn-c4-light {
            cursor: pointer;
            background: white;
            color: #54565a;
            /* border-radius: 15px; */
            border-color: #54565a;
        }

        .btn-c4-light:hover {
            color: white !important;
            border-color: #54565a;
            background: #54565a;
        }

        .fixed-height-wizard {
            height: 700px; /* Altura fija para el wizard */
            overflow-y: auto !important; /* Habilita el scroll interno si el contenido excede la altura fija */
        }

        @media (max-width: 768px) {
            .fixed-height-wizard {
                height: 100%; /* Ajuste para pantallas pequeñas */
                min-height: 300px; /* Altura mínima para dispositivos móviles */
            }
        }
    </style>

    <!-- App Css-->
    <link href="{{asset('css/app.css')}}" rel="stylesheet" />
</head>

<body>

    <div class="auth-page-wrapper auth-bg-cover d-flex justify-content-center align-items-center min-vh-100">
        <div class="auth-one-bg-position auth-one-bg" id="auth-particles" style="background-image: none;">
            <div class="bg-overlay bg-c2"></div>
            <div class="shape">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1440 120">
                    <path d="M 0,36 C 144,53.6 432,123.2 720,124 C 1008,124.8 1296,56.8 1440,40L1440 140L0 140z"></path>
                </svg>
            </div>
            <canvas class="particles-js-canvas-el" style="width: 100%; height: 100%;" width="1920" height="380"></canvas>
        </div>

        <!-- auth-page content -->
        <div class="auth-page-content overflow-hidden">
            <div class="container-fluid container-lg">
                <div class="row">
                    <div class="col-sm-12 d-none d-sm-block col-lg-6">
                        <div class="text-center">
                            <a href="#" class="auth-logo">
                                <img style="width: 400px;" src="{{asset('img/LOGOS-SET-RED.png')}}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-12 d-none d-sm-block col-lg-6">
                        <div class="text-center">
                            <a href="#" class=" auth-logo">
                                <img style="width: 400px;" src="{{asset('img/SIAN-TXRED.png')}}" alt="">
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card overflow-hidden fixed-height-wizard">
                    <div class="row g-0 h-100">
                        <div class="col-md-6">
                            <div class="auth-one-bg h-100  d-flex flex-column justify-content-end">
                                <div class="bg-overlay"></div>
                                <div class="position-relative">
                                    <!-- Carousel, extendido verticalmente -->
                                    <div class="p-lg-5 p-4">
                                        <!-- Aquí va el contenido del carousel -->
                                        <div id="qoutescarouselIndicators" class="carousel slide" data-bs-ride="carousel">
                                            <div class="carousel-indicators">
                                                <button type="button" data-bs-target="#qoutescarouselIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                <button type="button" data-bs-target="#qoutescarouselIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                <button type="button" data-bs-target="#qoutescarouselIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                            </div>
                                            <div class="carousel-inner text-center text-white pb-5">
                                                <div class="carousel-item active">
                                                    <p class="fs-15 fst-italic">Descargar manuales</p>
                                                </div>
                                                <div class="carousel-item">
                                                    <p class="fs-15 fst-italic">Reestablecer contraseña</p>
                                                </div>
                                                <div class="carousel-item">
                                                    <p class="fs-15 fst-italic">Mesa de ayuda</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 d-flex align-items-center p-lg-5 p-4">
                            <div class="w-100">
                                <form action="#" class="form-steps" autocomplete="off" id="form-registrar-usuario">
                                    <div class="text-center py-4">
                                        <h5>Registre su cuenta</h5>
                                    </div>
                                    <div id="custom-progress-bar" class="progress-nav mb-4">
                                        <div class="progress progress-c2" style="height: 5px;">
                                            <div class="progress-bar" role="progressbar" style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        <ul class="nav nav-pills progress-bar-tab custom-nav" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link rounded-pill active" data-progressbar="custom-progress-bar" id="pills-buscar-empleado-tab" data-bs-toggle="pill" data-bs-target="#pills-buscar-empleado" type="button" role="tab" aria-controls="pills-buscar-empleado" aria-selected="true" data-position="0">1</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link rounded-pill" data-progressbar="custom-progress-bar" id="pills-registrar-usuario-tab" data-bs-toggle="pill" data-bs-target="#pills-registrar-usuario" type="button" role="tab" aria-controls="pills-registrar-usuario" aria-selected="false" data-position="1" tabindex="-1" disabled>2</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link rounded-pill" data-progressbar="custom-progress-bar" id="pills-success-tab" data-bs-toggle="pill" data-bs-target="#pills-success" type="button" role="tab" aria-controls="pills-success" aria-selected="false" data-position="2" tabindex="-1" disabled>3</button>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="tab-content">
                                        <div class="tab-pane fade active show" id="pills-buscar-empleado" role="tabpanel" aria-labelledby="pills-buscar-empleado-tab">
                                            <div class="alert alert-c1" id="alerta-rfc" style="display:none;" role="alert">
                                                <div class="alert-body">
                                                    <div class="d-flex">
                                                        <div class="flex-shrink-0 me-3">
                                                            <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M12 9v4" /><path d="M10.363 3.591l-8.106 13.534a1.914 1.914 0 0 0 1.636 2.871h16.214a1.914 1.914 0 0 0 1.636 -2.87l-8.106 -13.536a1.914 1.914 0 0 0 -3.274 0z" /><path d="M12 16h.01" /></svg>
                                                        </div>
                                                        <div class="flex-grow-1">
                                                            <h6 class="alert-heading">¡Algo salió mal!</h6>
                                                            <p class="mb-0"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <p class="text-muted mb-3">
                                                1. Ingresa tu <em>RFC</em> en el campo de texto siguiente y luego da click en el botón que dice <strong>Buscar</strong>, junto al campo de texto.</li>
                                            </p>
                                            <div class="row">
                                                <div class="col mb-3">
                                                    <div class="form-floating">
                                                        <input type="text" class="form-control" id="rfc-input" name="rfc" pattern="[A-Za-z]{4}\d{6}[A-Za-z0-9]{0,3}" minlength="10" maxlength="13" placeholder="RFC" required>
                                                        <label for="rfc-input">RFC</label>
                                                        <span class="invalid-feedback"></span>
                                                    </div>
                                                </div>
                                                <div class="col-auto mb-3">
                                                    <button type="button" class="btn btn-c2-light h-100" id="btn-buscar-empleado">
                                                        <svg class="icon" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon"><path stroke="none" d="M0 0h24v24H0z" fill="none"></path><path d="M10 10m-7 0a7 7 0 1 0 14 0a7 7 0 1 0 -14 0"></path><path d="M21 21l-6 -6"></path></svg>
                                                        Buscar
                                                    </button>
                                                </div>

                                                <div class="col-12 mb-3" id="hint-cambiar-rfc" style="display:none;">
                                                    <div class="float-end">
                                                        <a href="#" onclick="habilitarInput()" style="color:#bc955c;">Haz click aquí para cambiarlo</a>
                                                    </div>
                                                    ¿No es tu RFC?
                                                </div>
                                            </div>
                                            <div class="d-flex align-items-start gap-3 mt-2">
                                                <button type="button" id="btn-continuar-registro" class="btn btn-c1-light w-100 nexttab" style="display:none;" data-nexttab="pills-registrar-usuario-tab">
                                                    Continuar
                                                    <svg class="align-middle" xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-arrow-right"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M5 12l14 0" /><path d="M13 18l6 -6" /><path d="M13 6l6 6" /></svg>
                                                </button>
                                            </div>
                                        </div>
                                        <!-- end tab pane -->

                                        <div class="tab-pane fade" id="pills-registrar-usuario" role="tabpanel" aria-labelledby="pills-registrar-usuario-tab">
                                            <div class="row">
                                                <div class="col-12 col-lg-6 col-xl-4 mb-3">
                                                    <div class="form-floating">
                                                        <input type="text" id="nombre-input" name="nombre" class="form-control" placeholder="Nombre" readonly>
                                                        <!-- <label for="nombre-input">Nombre</label> -->
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-6 col-xl-4 mb-3">
                                                    <div class="form-floating">
                                                        <input type="text" id="ape-pat-input" name="apepat" class="form-control" placeholder="Apellido Paterno" readonly>
                                                        <!-- <label for="ape-pat-input">Apellido Paterno</label> -->
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-6 col-xl-4 mb-3">
                                                    <div class="form-floating">
                                                        <input type="text" id="ape-mat-input" name="apemat" class="form-control" placeholder="Apellido Materno" readonly>
                                                        <!-- <label for="ape-mat-input">Apellido Materno</label> -->
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-6 col-xl-8 mb-3">
                                                    <div class="form-floating">
                                                        <input type="text" id="curp-input" name="curp" class="form-control" placeholder="Curp" readonly>
                                                        <!-- <label for="curp-input">Curp</label> -->
                                                    </div>
                                                </div>

                                                <div class="col-12 mb-3">
                                                    <div class="form-floating">
                                                        <input type="email" id="email-input" name="Email" class="form-control" placeholder="Dirección de correo electrónico" required>
                                                        <label for="email-input">Dirección de correo electrónico</label>
                                                        <span class="invalid-feedback"></span>
                                                    </div>
                                                </div>

                                                <div class="col-12 mb-3">
                                                    <div class="form-floating position-relative">
                                                        <input type="password" id="password-input" name="Password" class="form-control" placeholder="Contraseña" required>
                                                        <label for="password">Contraseña</label>
                                                        <span class="invalid-feedback"></span>
                                                    </div>
                                                </div>
                                                <div class="col-12 mb-3">
                                                    <div class="form-floating">
                                                        <input type="password" id="password-confirmation-input" name="PasswordConfirmation" class="form-control" placeholder="Confirmación de la contraseña" required>
                                                        <label for="password-confirmation-input">Confirmación de la contraseña</label>
                                                        <span class="invalid-feedback"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="d-flex align-items-start gap-3">
                                                <button type="button" class="btn btn-c4-light previestab" data-previous="pills-buscar-empleado-tab">
                                                    <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-arrow-left"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M5 12l14 0" /><path d="M5 12l6 6" /><path d="M5 12l6 -6" /></svg>
                                                    Volver
                                                </button>
                                                <button type="submit" form="form-registrar-usuario" class="btn btn-c1-light right ms-auto nexttab" id="btn-registrar-usuario" onclick="!this.form && document.getElementById('form-registrar-usuario').submit()">
                                                    Registrarme
                                                    <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-user"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M8 7a4 4 0 1 0 8 0a4 4 0 0 0 -8 0" /><path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2" /></svg>
                                                </button>
                                            </div>
                                        </div>
                                        <!-- end tab pane -->

                                        <div class="tab-pane fade" id="pills-success" role="tabpanel" aria-labelledby="pills-success-tab">
                                            <!-- El siguiente pane se debe crear de manera dinámica con base en las respuestas -->
                                            <div id="third-tab-content"></div>
                                            <div class="d-flex align-items-start gap-3 mt-4">
                                                <button type="button" class="btn btn-c4-light w-100 previestab" id="btn-prev-tab" data-previous="pills-registrar-usuario-tab">
                                                    Volver
                                                </button>
                                            </div>
                                        </div>
                                        <!-- end tab pane -->
                                    </div>
                                    <!-- end tab content -->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </div>
            <!-- end container -->
        </div>
        <!-- end auth page content -->
        <footer class="footer" style="background: #ab0033;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <p class="mb-0" style="color: white; font-size: 12px;">
                                Todos los derechos reservados © <script>document.write(new Date().getFullYear())</script> / Gobierno del Estado de Tamaulipas 2022 - 2028
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- end auth-page-wrapper -->

    <!-- JAVASCRIPT -->
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>

    <script src="{{asset('js/form-wizard.init.js')}}"></script>

    <script src="{{asset('js/particles.js')}}"></script>
    <script src="{{asset('js/particles.min.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        async function getData(url) {
            try {
                const response = await fetch(url, {method: 'get'});

                if (!response.ok) {
                    if (response.status === 404)
                    {
                        throw new Error('No se encontró el recurso que estás buscando.');
                    }
                    else if (response.status === 422)
                    {
                        console.error('Valida los campos ingresados.');
                    }
                    else
                    {
                        throw new Error('Hubo un problema al procesar la solicitud.');
                    }
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        async function postData(url, data) {
            try {
                const response = await fetch(url, {
                    method: 'POST',
                    headers: {
                        "X-Requested-With": "XMLHttpRequest",
                    },
                    body: data
                });

                if (!response.ok) {
                    if (response.status === 404)
                    {
                        throw new Error('No se encontró el recurso que estás buscando.');
                    }
                    else if (response.status === 422)
                    {
                        console.error('Valida los campos ingresados.');
                    }
                    else
                    {
                        throw new Error('Hubo un problema al procesar la solicitud.');
                    }
                }

                const result = await response.json();
                return result;
            } catch (error) {
                throw error;
            }
        }

        function validarRfc(rfc) {
            const pattern = /^[A-Za-z]{4}\d{6}[A-Za-z0-9]{0,3}$/;
            return pattern.test(rfc);
        }

        function errorAlert(message) {
            Swal.fire('¡Algo salió mal!', message, 'error');
        }

        function limpiarAlertaRfc()
        {
            const alerta = document.getElementById('alerta-rfc');
            alerta.querySelector("p").innerHTML="";
            alerta.style.display = "none";
        }

        function llenarAlertaRfc(mensaje)
        {
            const alerta = document.getElementById('alerta-rfc');
            alerta.querySelector("p").innerHTML=mensaje;
            alerta.style.display = "block";
        }

        function deshabilitarInput()
        {
            hintCambiarRfc.style.display = "block";

            // Deshabilitar el campo RFC y el botón de "Buscar" (btnBuscarEmpleado)
            // Establece el campo como solo lectura
            inputRfc.readOnly = true;

            // Darle la apariencia de un campo de texto deshabilitado
            inputRfc.classList.add("disabled");

            // Deshabilita eventos de puntero para que no se pueda interactuar con el campo
            inputRfc.style.pointerEvents = 'none';

            // Elimina el campo del orden de tabulación
            inputRfc.tabIndex = -1;

            // Botones
            btnBuscarEmpleado.disabled = true;

            btnContinuarRegistro.disabled = false;
            btnContinuarRegistro.style.display = "block";

            // Habilitar los campos del registro de usuarios
            // Nombre
            inputNombre.disabled = false;
            inputApePat.disabled = false;
            inputApeMat.disabled = false;
            inputCurp.disabled = false;

            // Credenciales
            inputEmail.disabled = false;
            inputPassword.disabled = false;
            inputPasswordConfirmation.disabled = false;

            btnRegistrarUsuario.disabled = false;

            // Habilitar la pestaña no. 2
            tabRegistrarUsuario.disabled = false;
        }

        function habilitarInput()
        {
            hintCambiarRfc.style.display = "none";

            // Habilitar el campo RFC y el botón de "Buscar" (btnBuscarEmpleado)
            inputRfc.readOnly = false;

            // Remover la apariencia de un campo de texto deshabilitado
            inputRfc.classList.remove("disabled");

            // Habilitar eventos de puntero para que no se pueda interactuar con el campo
            inputRfc.style.pointerEvents = 'auto';

            // Incluye el campo en el orden de tabulación
            inputRfc.tabIndex = 0;

            // Botones
            btnBuscarEmpleado.disabled = false;

            btnContinuarRegistro.disabled = true;
            btnContinuarRegistro.style.display = "none";

            // Limpiar y deshabilitar los campos del registro de usuarios
            // Nombre
            inputNombre.value = "";
            inputApePat.value = "";
            inputApeMat.value = "";
            inputCurp.value = "";

            inputNombre.disabled = true;
            inputApePat.disabled = true;
            inputApeMat.disabled = true;
            inputCurp.disabled = true;

            // Credenciales
            inputEmail.value = "";
            inputPassword.value = "";
            inputPasswordConfirmation.value = "";

            inputEmail.disabled = true;
            inputPassword.disabled = true;
            inputPasswordConfirmation.disabled = true;

            btnRegistrarUsuario.disabled = true;

            // Deshabilitar la pestaña no. 2
            tabRegistrarUsuario.disabled = true;

            // Limpiar la bolsa de errores del formulario de la pestaña no. 2
            clearInputErrors();

            // Deshabilitar y limpiar la pestaña no. 3
            tabSuccess.disabled = true;

            const thirdTabContent = document.getElementById('third-tab-content');

            // Limpia el contenido existente
            thirdTabContent.innerHTML = '';
        }

        function fillInputErrors(errors) {
            Object.keys(errors).forEach(function(key) {
                // Selecciona el input por su atributo name
                let input = formRegistrarUsuario.querySelector(`[name="${key}"]`);
                if (input) {
                    // Añadir la clase 'is-invalid' al input
                    input.classList.add('is-invalid');

                    // Buscar el span de feedback asociado
                    let span = input.closest('.form-floating').querySelector('.invalid-feedback');
                    if (span) {
                        span.classList.add('d-block');
                        span.textContent = errors[key];
                    }
                }
            });
        }

        function clearInputErrors() {
            // Selecciona todos los inputs que tienen la clase 'is-invalid'
            const invalidInputs = formRegistrarUsuario.querySelectorAll('.form-control.is-invalid');

            invalidInputs.forEach(function(input) {
                // Remueve la clase 'is-invalid' del input
                input.classList.remove('is-invalid');

                // Encuentra el span asociado con el mensaje de error
                let span = input.closest('.form-floating').querySelector('.invalid-feedback');
                if (span) {
                    // Oculta el mensaje de error y limpia su contenido
                    span.classList.remove('d-block');
                    span.textContent = "";
                }
            });
        }

        function thirdTab(status, message)
        {
            const thirdTabContent = document.getElementById('third-tab-content');

            const btnPreviousTab = document.getElementById('btn-prev-tab');

            // Limpia el contenido existente
            thirdTabContent.innerHTML = '';

            if (status === 'success') {
                btnPreviousTab.style.display = "none";

                const tabBuscarEmpleado = document.getElementById('pills-buscar-empleado-tab');
                tabBuscarEmpleado.disabled = true;
                tabRegistrarUsuario.disabled = true;

                thirdTabContent.innerHTML = `
                    <div class="alert alert-c2 alert-additional mb-xl-0" role="alert">
                        <div class="alert-body">
                            <div class="d-flex">
                                <div class="flex-shrink-0 me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon" style="color: #e3c8a3; width:70px; height:70px">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                        <path d="M12 12m-9 0a9 9 0 1 0 18 0a9 9 0 1 0 -18 0"></path>
                                        <path d="M9 12l2 2l4 -4"></path>
                                    </svg>
                                </div>
                                <div class="flex-grow-1">
                                    <h6 class="alert-heading">¡Registro Exitoso! 🎉</h6>
                                    <p class="mb-0">Para activar tu cuenta, por favor, verifica tu dirección de correo electrónico (<strong>${message}</strong>) haciendo click en el enlace que te enviamos.</p>
                                </div>
                            </div>
                        </div>
                        <div class="alert-content">
                            <p class="mb-0">
                                Revisa tu bandeja de entrada y sigue las instrucciones para confirmar tu cuenta. Si no encuentras el email en tu bandeja de entrada, te recomendamos revisar la carpeta de spam o correo no deseado.
                            </p>
                        </div>
                    </div>
                `;
            } else if (status === 'error') {
                btnPreviousTab.style.display = "block";

                thirdTabContent.innerHTML = `
                    <div class="alert alert-c1 mb-xl-0" role="alert">
                        <div class="alert-body">
                            <div class="d-flex">
                                <div class="flex-shrink-0 me-3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                                        <path d="M12 9v4"></path>
                                        <path d="M10.363 3.591l-8.106 13.534a1.914 1.914 0 0 0 1.636 2.871h16.214a1.914 1.914 0 0 0 1.636 -2.87l-8.106 -13.536a1.914 1.914 0 0 0 -3.274 0z"></path>
                                        <path d="M12 16h.01"></path>
                                    </svg>
                                </div>
                                <div class="flex-grow-1">
                                    <h6 class="alert-heading">¡Algo salió mal!</h6>
                                    <p class="mb-0">${message}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            }
        }

        const formRegistrarUsuario = document.getElementById("form-registrar-usuario");

        // Deshabilitar por defecto las pestañas no. 2 y 3 del asistente
        const tabRegistrarUsuario = document.getElementById("pills-registrar-usuario-tab");
        const tabSuccess = document.getElementById("pills-success-tab");

        tabRegistrarUsuario.disabled = true;
        tabSuccess.disabled = true;

        // Selección de los elementos de la pestaña No. 1
        const inputRfc = document.getElementById("rfc-input");

        const btnBuscarEmpleado = document.getElementById("btn-buscar-empleado");

        // Ocultar la pista para cambiar el RFC
        const hintCambiarRfc = document.getElementById("hint-cambiar-rfc");
        hintCambiarRfc.style.display = "none";

        // Ocultar el botón para continuar pestaña no. 2 del formulario
        const btnContinuarRegistro = document.getElementById("btn-continuar-registro");
        btnContinuarRegistro.style.display = "none";

        // Selección de los elementos de pestaña No. 2
        // Nombre
        const inputNombre = document.getElementById("nombre-input");
        const inputApePat = document.getElementById("ape-pat-input");
        const inputApeMat = document.getElementById("ape-mat-input");
        const inputCurp = document.getElementById("curp-input");

        // Credenciales
        const inputEmail = document.getElementById("email-input");
        const inputPassword = document.getElementById("password-input");
        const inputPasswordConfirmation = document.getElementById("password-confirmation-input");

        const btnRegistrarUsuario = document.getElementById("btn-registrar-usuario");
        btnRegistrarUsuario.disabled = true;
    </script>

    <script>
        document.addEventListener("DOMContentLoaded", function() {
            // Alerta de error
            // Alerta pestaña no. 1 (formulario para Buscar Empleado)
            limpiarAlertaRfc();

            // Evento onClick del botón "Buscar" (btnBuscarEmpleado)
            btnBuscarEmpleado.addEventListener('click', () => {
                let rfc = inputRfc.value;

                // MOSTRAR VENTANA DE ESPERA
                if (rfc) {
                    if(!validarRfc(rfc)){
                        llenarAlertaRfc("El RFC ingresado no es válido.");
                        return;
                    }

                    const url = `http://localhost:5132/RegistrarUsuario/${rfc}`;

                    getData(url)
                    .then(result => {
                        if(result.log && !result.log.flag) {
                            llenarAlertaRfc(result.log.mensaje);
                            habilitarInput();
                        }
                        else {
                            if(result.empleado){
                                const nombre = result.empleado.nombre;
                                const apPaterno = result.empleado.ap_paterno;
                                const apMaterno = result.empleado.ap_materno;
                                const curp = result.empleado.curp;

                                rfc = result.empleado.rfc;

                                if(rfc){
                                    inputRfc.value = rfc
                                    inputNombre.value = nombre;
                                    inputApePat.value = apPaterno;
                                    inputApeMat.value = apMaterno;
                                    inputCurp.value = curp;

                                    limpiarAlertaRfc();

                                    // Deshabilitar el campo de texto para ingresar el RFC
                                    deshabilitarInput();
                                }
                            }
                        }
                    })
                    .catch(error => errorAlert(error.message));
                } else {
                    llenarAlertaRfc("Por favor, ingrese su RFC.");
                }
            });

            formRegistrarUsuario.addEventListener("submit", function(event) {
                event.preventDefault();

                const formData = new FormData(this);

                const rfc = formData.get('rfc');

                // MOSTRAR VENTANA DE ESPERA
                if (rfc) {
                    if(!validarRfc(rfc)){
                        llenarAlertaRfc("El RFC ingresado no es válido.");
                        habilitarInput();
                    }

                    postData('https://localhost:7120/RegistrarUsuario/', formData)
                    .then(result => {

                        clearInputErrors();

                        if(result.errors)
                        {
                            fillInputErrors(result.errors);
                        }
                        else
                        {
                            if(result.log){
                                if (result.log.flag) {
                                    thirdTab('success', result.log.mensaje);
                                } else {
                                    thirdTab('error', result.log.mensaje);
                                }
                            }

                            tabSuccess.disabled = false;
                            tabSuccess.click();
                        }
                    })
                    .catch(error => errorAlert(error.message));
                } else {
                    llenarAlertaRfc("Por favor, ingrese su RFC.");
                    habilitarInput();
                }
            });
        });
    </script>
</body>
</html>
