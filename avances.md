# 2023-11-21
- [x] concatenar el nombre completo en el modelo User con un accesor

# 2023-11-22
*- [ ] buscar cómo hacer consultas eficientes con Eloquent*

# 2023-11-23
- [x] llenar registros de usuarios con factorías

*- [ ] hacer testing*

# 2023-11-28
Implementar sistema de autenticación y terminar de preparar la plantilla (mostrar)

- [ ] implementar la funcionalidad del primer inicio de sesión para cifrar la contraseña

- [ ] Implementar el paquete de Laravel Permissions

[Última página del blog Laravel Daily](https://laraveldaily.com/?page=52)

[Roadmap](https://laraveldaily.com/roadmap-learning-path)

# 2023-11-29
[Choosing the Right Pagination Strategy in Laravel: Cursor vs. Offset Pagination](https://devkeytech.medium.com/choosing-the-right-pagination-strategy-in-laravel-cursor-vs-offset-pagination-e6b6c6494955)

# 2023-11-30

# 2023-12-08 (*Check*)
Para añadir opciones en la librería Tom Select, se pueden obtener con el método select() y pluck()

Es preferible utilizar *select* cuando las consultas son más elaboradas.

Los plugins que me resultan útiles en la librería Tom Select son los siguientes: Remove Button, Caret Position,

# 2023-12-11
- [x] login
- [ ] view settings (columnas de las tablas)
- [ ] añadir los local scopes
- [ ] buscar convenciones para hacer commits
- [ ] problemas con la función clearFields() [se debe utilizar la función propia de la librería tom-select para limpiar el selector] clearInputErrors() [problemas con nextElementSibling]

# 2023-12-12
- [ ] diseñar aplicación para finanzas personales

gastos (compras [menaje], comida, pagos, deudas, recibos)

# 2023-12-13
- [x] ~~implementar la paginación en las listas de roles y permisos~~
- [ ] dejar pendiente el acceso en el listado de roles para ver los usuarios que tienen determinado rol. Planearse que quizás no es óptimo
  - [ ] pendiente ver una interfaz gráfica convincente para modelar la lista de usuarios que se les puede asignar un rol

# 2023-12-19
- [ ] ajustar las dimensiones de las columnas (responsividad y ajuste de las tablas a resoluciones más pequeñas)

¿Tiene sentido restringir la búsqueda en una vista que muestra información?

- [ ] permiso de usuarios: reiniciar contraseña

- [ ] implementar que en el primer inicio de sesión el usuario cambie su contraseña

# 2024-01-08
- [ ] implementar la paginación 


La restricción de un permiso consiste en:

- prohibir la url de determinada página (middlware)
- ocultar el botón o enlace que dirige hacia determinada página

---
Menu 
¿Tiene hijos?

¿Está relacionado con un permiso?

Orden:
- [ ] Crear el proyecto con la versión de su preferencia
- [ ] Modificar el lenguaje y la zona horaria
- [ ] Modificar el modelo User (y migración). También contemplar el eliminado lógico

- [ ] Template y recursos de este
  
- [ ] Instalar el paquete Laravel Fortify (sistema de autenticación agnóstico)
- [ ] Definir las funcionalidades que se van a utilizar del sistema de autenticación (Login, registro de usuarios, etc.)
- [ ] Creación de las vistas funcionales las funcionalidades del sistema de autenticación
- [ ] 


La creación de menús dinámicos queda pendiente hasta que se analice más a detalle
