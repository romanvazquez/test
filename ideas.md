# Inspiraciones
## [Belich](https://www.belich.dev/)

## Laravel + Vue + Vuetify Admin Dashboard by Jigesh Raval
[Video demo](https://www.youtube.com/watch?v=ObElAwljgdo) + [Github repository](https://github.com/jigeshraval/laravel-admin-dashboard)

- [ ] bitácora de errores y acciones (inicios de sesión, operaciones en el controlador, etc.)
- [ ] eliminado lógico de recursos

# Operaciones CRUD

## Visualización de datos

### Controles de búsqueda

- [ ] Controles persistentes a través de la URL de la página

- [ ] Personalizar las columnas de la tabla sobre las cuales pueden realizar búsquedas y ordenamientos

### Búsqueda
Las propiedades a partir de las cuales se pueden hacer búsquedas deben estar definidas.

Objetivo: crear búsquedas dinámicas (seleccionar los campos que deben filtrar)

### Filtros y ordenamiento
Para filtrar resultados que se muestran en una tabla, los atributos a partir de los cuales se puede realizar el filtro primordialmente deben ser catálogos

El ordenamiento debe darse a partir de atributos definidos.

### Paginación

# Proyectos
## Sistema de autenticación
### Login
### Register

## Sistema de Roles y Permisos

## Sistema rastreador de Estadísticas (Laravel Stats)
- [ ] [Lectura interesante](https://www.reddit.com/r/laravel/comments/vx7awo/caching_every_query_result_is_that_efficient/)

## Blog

## Mesa de ayuda

## Ecommerce

# Funcionalidades

## Control de notificaciones

- [ ] validar los parámetros de las rutas (tipo de dato) en el backend
- [ ] validar y sanitizar los parámetros del Request
    - [ ] frontend (con expresiones regulares)
    - [ ] backend
- [ ] darle un mensaje de error genérico al método 'store' del controlador. Preferiblemente después de contemplar la bitácora (o log de errores)

- [ ] ver cómo se van a recuperar los botones de acciones cuando la pantalla se haga pequeña
- [ ] revisar cómo están acomodados los botones de acciones en el proyecto que ya había hecho
- [ ] faltan las políticas y gateway para autorizar la modificación de los recursos

# futuro
*- [ ] indentificar y escribir los atributos de accesibilidad*

*- [ ] personalización de búsqueda genérica (replicar el estilo de programación de Spatie)*
