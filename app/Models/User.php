<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles, HasApiTokens, HasFactory, Notifiable;

    // Model column alias
    protected $maps = [
        'ape_pat' => 'paterno',
        'ape_mat' => 'materno',

        // 'username' => 'usuario',
        // 'is_active' => 'estatus',
        // 'is_password_updated' => 'ingreso',
    ];

    protected $appends = [ 'paterno', 'materno' ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nombre',
        'ape_pat',
        'ape_mat',
        'username',
        'is_active',
        'is_password_updated',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',

        'ape_pat',
        'ape_mat',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNombreCompletoAttribute(): string
    {
        return "{$this->nombre} {$this->ape_pat} {$this->ape_mat}";
    }

    public function getPaternoAttribute()
    {
        return $this->attributes['ape_pat'];
    }

    public function getMaternoAttribute()
    {
        return $this->attributes['ape_mat'];
    }

    // Método de acceso para obtener las iniciales de nombre y apellido
    public function getInicialesAttribute()
    {
        $iniciales = substr($this->nombre, 0, 1);

        if ($this->ape_pat) {
            $iniciales .= substr($this->ape_pat, 0, 1);
        } elseif ($this->ape_mat) {
            $iniciales .= substr($this->ape_mat, 0, 1);
        }

        return strtoupper($iniciales);
    }
}
