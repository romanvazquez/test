<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = $this->route('user');

        return [
            'nombre' => 'required|string|min:2|max:60',
            'paterno' => 'nullable|string|min:2|max:60',
            'materno' => 'nullable|string|min:2|max:60',
            'email' => 'required|string|email|max:255|unique:users,email,'.$user_id,
            'username' => 'required|string|max:60|unique:users,username,'.$user_id
        ];
    }
}
