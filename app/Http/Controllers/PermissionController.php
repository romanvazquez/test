<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Permission;

// use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB; // Raw queries

// Request
use App\Http\Requests\StorePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;

// Excepciones
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class PermissionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePermissionRequest $request)
    {
        try {
            Permission::create(['name' => $request->name]);

            return response()->json([
                'message' => 'El permiso se ha creado.'
            ], 200);
        } catch (QueryException $e) {

            // Capturar estos errores en una bitácora
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = DB::table('permissions')->select('id', 'name')->where('id', $id)->first();

        if(!$permission){
            return response()->json(['message' => 'El permiso no se ha no encontrado.'], 404);
        }

        return response()->json($permission);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePermissionRequest $request, $id)
    {
        try {
            $permission = Permission::findOrFail($id);

            $permission->update([
                'name' => $request->name
            ]);

            return response()->json([
                'message' => 'El permiso se ha actualizado.'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Recurso no encontrado.'], 404);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['message' => 'Ocurrio un error al procesar la solicitud.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
