<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePageActionController extends Controller
{

    // UPDATE tu_tabla SET tu_columna = REPLACE(REPLACE(tu_columna, '\r', ''), '\n', '');

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        // $roles = Role::withCount('users')->paginate(
        //     $perPage = 10, $columns = ['*'], $pageName = 'rolesPage'
        // );

        $roles = Role::paginate(
            $perPage = 10, $columns = ['*'], $pageName = 'rolesPage'
        );

        $permissions = Permission::paginate(
            $perPage = 10, $columns = ['*'], $pageName = 'permissionsPage'
        );

        // $users = DB::select("SELECT id, CONCAT_WS(' ', COALESCE(nombre, ''), COALESCE(ape_pat, ''), COALESCE(ape_mat, '')) AS nombre
        //     FROM users
        //     ORDER BY nombre");

        // $usersOpt = [];

        // foreach ($users as $user) {
        //     $primerLetra = strtoupper(substr($user->nombre, 0, 1));
        //     $usersOpt[$primerLetra][] = $user;
        // }

        // $permissionsOpt = DB::select("SELECT
        //         SUBSTRING_INDEX(name, ':', 1) AS resource,
        //         COUNT(*) AS total,
        //         NULL AS C,
        //         NULL AS R,
        //         NULL AS U,
        //         NULL AS D,
        //         NULL total_others,
        //         NULL other_permissions
        //     FROM permissions
        //     GROUP BY resource
        //     ORDER BY total DESC;");

        // $operations = [
        //     'C' => 'create%',
        //     'R' => 'read%',
        //     'U' => 'update%',
        //     'D' => 'delete%'
        // ];

        // foreach ($permissionsOpt as $permission) {
        //     $resource = $permission->resource;

        //     foreach ($operations as $key => $operation) {
        //         $str = $resource . ":" . $operation;
        //         $tmp = DB::table('permissions')->where('name', 'LIKE', $str)->value('id');
        //         $permission->$key = $tmp;
        //     }

        //     $query = DB::table('permissions')
        //         ->selectRaw('IFNULL(JSON_ARRAYAGG(JSON_OBJECT("id", id, "name", name)), "[]") AS other_permissions')
        //         ->selectRaw('IFNULL(COUNT(*), 0) AS total_others')
        //         ->where('name', 'LIKE', $resource . ':%')
        //         ->whereNotIn(DB::raw('SUBSTRING_INDEX(name, ":", -1)'), ['create', 'update', 'delete'])
        //         ->whereNotNull(DB::raw('SUBSTRING_INDEX(name, ":", -1)'))
        //         ->first();

        //     $permission->other_permissions = json_decode($query->other_permissions, true);
        //     $permission->total_others = $query->total_others;
        // }

        // return view('roles-permissions.index', compact('roles', 'permissions', 'permissionsOpt', 'usersOpt'));

        return view('roles-permissions.index', compact('roles', 'permissions'));
    }
}
