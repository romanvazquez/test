<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// use Spatie\QueryBuilder\QueryBuilder;

class UserController extends Controller
{
    public function foo($id)
    {
        $query = DB::select("SELECT id_cons_anex2, Concepto FROM Anexo_3 WHERE Concepto IS NOT NULL;");
        $query = DB::select("SELECT id_cons_anex2, Modelos FROM Anexo_3 WHERE Modelos IS NOT NULL;");
        $query = DB::select("SELECT id_cons_anex2, Requisitos FROM Anexo_3 WHERE Requisitos IS NOT NULL;");
        $query = DB::select("SELECT id_cons_anex2, Fundamento FROM Anexo_3 WHERE Fundamento IS NOT NULL;");
        $query = DB::select("SELECT id_cons_anex2, Excepciones FROM Anexo_3 WHERE Excepciones IS NOT NULL;");
        $query = DB::select("SELECT id_cons_anex2, Periodicidad FROM Anexo_3 WHERE Periodicidad IS NOT NULL;");
        $query = DB::select("SELECT id_cons_anex2, Observaciones FROM Anexo_3 WHERE Observaciones IS NOT NULL;");
        $query = DB::select("SELECT id_cons_anex2, Consideraciones_de_la_Entidad_Federativa AS Consideraciones FROM Anexo_3 WHERE Consideraciones_de_la_Entidad_Federativa IS NOT NULL;");

        $concepto = DB::select("SELECT id_cons_anex2, Concepto FROM Anexo_3 WHERE id_cons_anex2 = :id_cons_anex2;", ['id_cons_anex2' => $id]);
        $modelos = DB::select("SELECT id_cons_anex2, Modelos FROM Anexo_3 WHERE id_cons_anex2 = :id_cons_anex2;", ['id_cons_anex2' => $id]);
        $requisitos = DB::select("SELECT id_cons_anex2, Requisitos FROM Anexo_3 WHERE id_cons_anex2 = :id_cons_anex2;", ['id_cons_anex2' => $id]);
        $fundamento = DB::select("SELECT id_cons_anex2, Fundamento FROM Anexo_3 WHERE id_cons_anex2 = :id_cons_anex2;", ['id_cons_anex2' => $id]);
        $excepciones = DB::select("SELECT id_cons_anex2, Excepciones FROM Anexo_3 WHERE id_cons_anex2 = :id_cons_anex2;", ['id_cons_anex2' => $id]);
        $periodicidad = DB::select("SELECT id_cons_anex2, Periodicidad FROM Anexo_3 WHERE id_cons_anex2 = :id_cons_anex2;", ['id_cons_anex2' => $id]);
        $observaciones = DB::select("SELECT id_cons_anex2, Observaciones FROM Anexo_3 WHERE id_cons_anex2 = :id_cons_anex2;", ['id_cons_anex2' => $id]);
        $consideraciones = DB::select("SELECT id_cons_anex2, Consideraciones_de_la_Entidad_Federativa AS Consideraciones FROM Anexo_3 WHERE id_cons_anex2 = :id_cons_anex2;", ['id_cons_anex2' => $id]);

        return view('anexo', [
            'consecutivo' => $id,
            'concepto' => $concepto[0]->Concepto,
            'modelos' => $modelos[0]->Modelos,
            'requisitos' => $requisitos[0]->Requisitos,
            'fundamento' => $fundamento[0]->Fundamento,
            'excepciones' => $excepciones[0]->Excepciones,
            'periodicidad' => $periodicidad[0]->Periodicidad,
            'observaciones' => $observaciones[0]->Observaciones,
            'consideraciones' => $consideraciones[0]->Consideraciones
        ]);
    }

    // // public function updateModelos(Request $request, $id){
    // //     $modelos = $request->modelos;
    // //     DB::update("UPDATE Anexo_3 SET modelos = ':modelos' WHERE id_cons_anex2 = :id_cons_anex2", [
    // //         'modelos' => $modelos,
    // //         'id_cons_anex2' => $id
    // //     ]);
    // // }

    public function updateRequisitos(Request $request, $id){
        $requisitos = $request->requisitos;
        try {
            DB::update('EXEC zz_Sp_Requisitos_Updt ?,?,?',[1, $id, $requisitos]);
            // DB::update("UPDATE Anexo_3 SET requisitos = :requisitos WHERE id_cons_anex2 = :id_cons_anex2", [
            //     'requisitos' => $requisitos,
            //     'id_cons_anex2' => $id
            // ]);
            return redirect()->route('foo.index', $id)->with('success', 'El registro se ha modificado satisfactoriamente.');
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function updateFundamento(Request $request, $id){
        $fundamento = $request->fundamento;
        try {
            DB::update('EXEC zz_Sp_Fundamento_Updt ?,?,?',[1, $id, $fundamento]);
            // DB::update("UPDATE Anexo_3 SET fundamento = :fundamento WHERE id_cons_anex2 = :id_cons_anex2", [
            //     'fundamento' => $fundamento,
            //     'id_cons_anex2' => $id
            // ]);
            return redirect()->route('foo.index', $id)->with('success', 'El registro se ha modificado satisfactoriamente.');
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function updateExcepciones(Request $request, $id){
        $excepciones = $request->excepciones;
        try {
            DB::update('EXEC zz_Sp_Excepciones_Updt ?,?,?',[1, $id, $excepciones]);
            // DB::update("UPDATE Anexo_3 SET excepciones = :excepciones WHERE id_cons_anex2 = :id_cons_anex2", [
            //     'excepciones' => $excepciones,
            //     'id_cons_anex2' => $id
            // ]);
            return redirect()->route('foo.index', $id)->with('success', 'El registro se ha modificado satisfactoriamente.');
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function updatePeriodicidad(Request $request, $id){
        $periodicidad = $request->periodicidad;
        try {
            DB::update('EXEC zz_Sp_Periodicidad_Updt ?,?,?',[1, $id, $periodicidad]);
            // DB::update("UPDATE Anexo_3 SET periodicidad = :periodicidad WHERE id_cons_anex2 = :id_cons_anex2", [
            //     'periodicidad' => $periodicidad,
            //     'id_cons_anex2' => $id
            // ]);
            return redirect()->route('foo.index', $id)->with('success', 'El registro se ha modificado satisfactoriamente.');
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function updateObservaciones(Request $request, $id){
        $observaciones = $request->observaciones;
        try {
            DB::update('EXEC zz_Sp_Observaciones_Updt ?,?,?',[1, $id, $observaciones]);
            // DB::update("UPDATE Anexo_3 SET observaciones = :observaciones WHERE id_cons_anex2 = :id_cons_anex2", [
            //     'observaciones' => $observaciones,
            //     'id_cons_anex2' => $id
            // ]);
            return redirect()->route('foo.index', $id)->with('success', 'El registro se ha modificado satisfactoriamente.');
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function updateConsideraciones(Request $request, $id){
        $consideraciones = $request->consideraciones;
        try {
            DB::update('EXEC zz_Sp_Consideraciones_Updt ?,?,?',[1, $id, $consideraciones]);
            // DB::update("UPDATE Anexo_3 SET consideraciones_de_la_entidad_federativa = :consideraciones WHERE id_cons_anex2 = :id_cons_anex2", [
            //     'consideraciones' => $consideraciones,
            //     'id_cons_anex2' => $id
            // ]);
            return redirect()->route('foo.index', $id)->with('success', 'El registro se ha modificado satisfactoriamente.');
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }



















    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $perPage = $request->input('perPage', 5);

        // $users = User::latest()->paginate($perPage);

        // // Añadir el parámetro 'perPage' a los enlaces de paginación
        // $users->appends(['perPage' => $perPage]);

        $users = User::latest()->get();

        return view('users.index', compact('users'));
    }

    // /**
    //  * Show the form for creating a new resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        try {
            User::create([
                'nombre' => $request->nombre,
                'ape_pat' => $request->paterno,
                'ape_mat' => $request->materno,
                'email' => $request->email,
                'username' => $request->username,
                'password' => $request->password
            ]);

            return response()->json([
                "status" => "success",
                "message" => "El usuario se ha creado correctamente."
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "status" => "error",
                // 'message' => $th->getMessage()
                "message" => "Hubo un problema al crear el usuario. Por favor, inténtalo de nuevo."
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::findOrFail($id);
            return response()->json($user);
        }
        catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'El usuario no se ha no encontrado.'], 404);
        }
    }

    // public function existUser(int $id)
    // {
    //     if (User::where('id', $id)->exists()) {
    //             // Eventualmente también se validará que el usuario tenga permisos para eliminar este recurso, etc.
    //         return response()->json(['existe' => true], 200);
    //     }

    //     return response()->json(['existe' => false], 404);
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        try {
            $user = User::findOrFail($id);

            $user->update([
                'nombre' => $request->nombre,
                'ape_pat' => $request->paterno,
                'ape_mat' => $request->materno,
                'email' => $request->email,
                'username' => $request->username
            ]);

            return response()->json([
                'message' => 'El usuario se ha actualizado.'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Recurso no encontrado.'], 404);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['message' => 'Ocurrio un error al procesar la solicitud.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);

            $user->delete();

            return response()->json([
                'message' => 'El usuario se ha eliminado.'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Recurso no encontrado.'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Ocurrio un error al procesar la solicitud.'], 500);
        }
    }
}
