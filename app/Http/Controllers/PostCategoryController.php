<?php

namespace App\Http\Controllers;

use App\Models\PostCategory;
use App\Http\Requests\StorePostCategoryRequest;
use App\Http\Requests\UpdatePostCategoryRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class PostCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $query = DB::select('CALL Sp_Post_Categories_Sel()');

        $post_categories = $this->buildTree($query);

        $categories_options = DB::select('CALL Sp_Post_Categories_Options_Sel(NULL)');

        // return $post_categories;

        return view('post-categories.index', compact('post_categories', 'categories_options'));
    }

    private function buildTree($categories, $parentId = null)
    {
        $branch = [];

        foreach ($categories as $category) {
            if ($category->parent_id == $parentId) {
                $children = $this->buildTree($categories, $category->id);

                if ($children) {
                    $category->children = $children;
                }

                $branch[] = $category;
            }
        }

        return $branch;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePostCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostCategoryRequest $request)
    {
        try {
            PostCategory::create([
                'title' => $request->title,
                'parent_id' => $request->parent_id
            ]);

            return response()->json([
                'message' => 'La categoría se ha creado.'
            ], 200);
        } catch (QueryException $e) {

            // Capturar estos errores en una bitácora
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function show(PostCategory $post_category)
    {
        if(!$post_category){
            return response()->json(['message' => 'El permiso no se ha no encontrado.'], 404);
        }

        return response()->json($post_category);
    }

    public function getCategories(PostCategory $post_category)
    {
        $id = $post_category->id;

        $post_categories = DB::select("CALL Sp_Post_Categories_Options_Sel(:id)", ['id' => $id]);

        return $post_categories;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(PostCategory $postCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePostCategoryRequest  $request
     * @param  \App\Models\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostCategoryRequest $request, $id)
    {
        try {
            $post_category = PostCategory::findOrFail($id);

            $post_category->update([
                'title' => $request->title,
                'parent_id' => $request->parent_id,
            ]);

            return response()->json([
                'message' => 'El permiso se ha actualizado.'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Recurso no encontrado.'], 404);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['message' => 'Ocurrio un error al procesar la solicitud.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PostCategory  $postCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostCategory $postCategory)
    {
        //
    }
}
