<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignPermissionsToRoleRequest;
use App\Http\Requests\AssignRoleToUsersRequest;
use App\Models\User;
use Spatie\Permission\Models\Role;

// use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB; // Raw queries

// Request
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;

// Excepciones
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

// use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        try {
            Role::create(['name' => $request->name]);

            return response()->json([
                'message' => 'El rol se ha creado.'
            ], 200);
        } catch (QueryException $e) {
            // Capturar estos errores en una bitácora (pendiente)
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = DB::table('roles')->select('id', 'name')->where('id', $id)->first();

        if(!$role){
            return response()->json(['message' => 'El rol no se ha no encontrado.'], 404);
        }

        return response()->json($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        try {
            $role = Role::findOrFail($id);

            $role->update([
                'name' => $request->name
            ]);

            return response()->json([
                'message' => 'El rol se ha actualizado.'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Recurso no encontrado.'], 404);
        } catch (\Illuminate\Database\QueryException $e) {
            return response()->json(['message' => 'Ocurrio un error al procesar la solicitud.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getUsersByRole($roleId)
    {
        try {
            // Obtener IDs de usuarios que tienen el rol específico
            $users = User::whereHas('roles', function ($query) use ($roleId) {
                $query->where('role_id', $roleId);
            })->pluck('id');

            // Si no se encuentran usuarios, verificar si el rol existe
            if ($users->isEmpty() && !Role::find($roleId)) {
                return response()->json(['message' => 'El rol especificado no existe.'], 404);
            }

            return response()->json([
                'roleId' => $roleId,
                'users' => $users
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'El rol especificado no existe.'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Ha ocurrido un error al procesar la solicitud.'], 500);
        }
    }

    public function assignRoleToUsers(AssignRoleToUsersRequest $request)
    {
        try {
            $role_id = $request->input('roleId');
            $user_ids = $request->input('userIds', []); // Por defecto es un array vacío

            $role = Role::findOrFail($role_id);

            // Sincroniza los usuarios para el rol específico
            $role->users()->sync($user_ids);

            return response()->json([
                'message' => 'Los cambios han sido guardados satisfactoriamente.'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'El rol indicado no existe.'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Ha ocurrido un error al procesar la solicitud.'], 500);
        }
    }

    public function getPermissionsByRole($role_id)
    {
        try {
            // Busca el rol por ID
            $role = Role::findOrFail($role_id);

            // Recupera los IDs de los permisos asociados al rol
            $permissions = $role->permissions()->pluck('id');

            return response()->json([
                'roleId' => $role_id,
                'permissions' => $permissions
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'El rol especificado no existe.'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Ha ocurrido un error al procesar la solicitud.'], 500);
        }
    }

    public function assignPermissionsToRole(AssignPermissionsToRoleRequest $request)
    {
        try {
            $role_id = $request->input('roleId');

            $permission_ids = array_map('intval', $request->input('permissionIds', []));

            $role = Role::find($role_id);

            $role->syncPermissions($permission_ids);

            return response()->json([
                'message' => 'Los cambios han sido guardados satisfactoriamente.'
            ], 200);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'El rol indicado no existe.'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
            // return response()->json(['message' => 'Ha ocurrido un error al procesar la solicitud.'], 500);
        }
    }
}
